# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :action_log do
    user_token "MyString"
    action "MyString"
    target "MyString"
    referrer "MyString"
    version "MyString"
    ip "MyString"
    date "MyString"
    locale "MyString"
  end
end
