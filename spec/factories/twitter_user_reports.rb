# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :twitter_user_report do
    user_id ""
    screen_name "MyString"
    friends_count ""
    following_count ""
    profile_image_url "MyString"
    feed_links ""
    statistics ""
  end
end
