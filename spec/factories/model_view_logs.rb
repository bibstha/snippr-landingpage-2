# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :model_view_log do
    type ""
    type_token "MyString"
    user_token "MyString"
  end
end
