require 'spec_helper'

describe "Authentications" do

  subject { page }
  
  describe "Visit SignIn Page" do
    before { visit new_user_session_path }
    it { should have_content("Sign in") }
    it { should have_field(:user_email) }
    it { should have_field(:user_password) }
    it { should have_selector("input[type=submit]") }

    describe "provide empty credentials" do
      before do
        click_on "Sign in"
      end
      it { should have_content "Invalid email or password." }
    end

    describe "provide wrong credentials" do
      before do
        user = FactoryGirl.create(:user)
        fill_in :user_email, :with => "testuser@example.com"
        fill_in :user_password, :with => "wrongpassword"
        click_on "Sign in"
      end
      it { should have_content "Invalid email or password" }
    end

    describe "login with valid credentials" do
      before do
        user = FactoryGirl.create(:user, email: "testuser@example.com", password: "secretpassword")
        fill_in :user_email, :with => "testuser@example.com"
        fill_in :user_password, :with => "secretpassword"
        click_on "Sign in"
      end
      it { should_not have_content "Invalid email or password" }

      describe "should redirect to authors page to follow authors" do
        it { current_path.should == "/authors" }
      end
    end

    describe "when user is already following authors" do
      let(:profile) { FactoryGirl.create(:profile) }
      let(:user) do
        FactoryGirl.create(:user, 
          email: "testuser@example.com", password: "secretpassword",
          following_profiles: [profile.token]
        )
      end

      before do
        fill_in :user_email, :with => user.email
        fill_in :user_password, :with => "secretpassword"
        click_on "Sign in"
      end
      
      it { current_path.should == user_news_feed_path(user.token) }
    end
  end
end
