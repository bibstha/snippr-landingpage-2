require 'spec_helper'
include Warden::Test::Helpers
Warden.test_mode!

describe "FollowProfiles" do
  subject { page }
  
  let(:profile1) { FactoryGirl.create(:profile, name: "Profile1 Name") }
  let(:profile2) { FactoryGirl.create(:profile, name: "Profile2 Name") }
  let(:profile3) { FactoryGirl.create(:profile, name: "Profile3 Name") }

  before "initialize profiles" do
    profile1
    profile2
    profile3
  end

  describe "logged out user tries to access follow profile" do
    it "should not allow the request" do
      xhr :post, author_follow_path(token: profile1.token)
      response.status.should eql 401
      response.body.should eql "You need to sign in or sign up before continuing."
    end
  end

  describe "loggedinuser" do
    let (:user) { FactoryGirl.create(:user) }

    describe "follow valid author", js: true do
      before do
        login_as(user)
        visit author_list_path 
      end
      
      it "should have all authors listed with follow button" do
        should have_content("All Authors") 

        should have_link(profile1.name)
        should have_link(profile2.name)
        should have_link(profile3.name)

        
      end

      it "when follows an author, is subscribed to him/her" do
        all(".btn-author-follow").first.click
        puts body
        puts user.to_json
        user.following_profiles.should include("Profile1 Name")
      end


      # it "follow author twice" do
      #   xhr :post, author_follow_path(token: profile1.token)
      #   response.body.should eql "{status: 'ok', message: 'unfollowing author', value: 0}"
      #   user.following_profiles.should_not include(profile1.token)
      # end
    end

    # describe "follow invalid author" do

  end
    
end
