require 'spec_helper'
include Warden::Test::Helpers
Warden.test_mode!

describe "User Pages" do
  
  subject { page }

  describe "GET /users/sign_up" do
    before { visit new_user_registration_path }
    it { status_code.should == 200 }
    it { should have_content("Sign up") }

    it { should have_field("user_display_name") }
    it { should have_field("user_email") }
    it { should have_field("user_password") }
    it { should have_field("user_password_confirmation") }
    it { should have_selector("input[type=submit]") }

    describe "when creating a new account" do
      before do
        fill_in :user_display_name, :with => "test_user"
        fill_in :user_email, :with => "test_user@example.com"
        fill_in :user_password, :with => "testpassword"
        fill_in :user_password_confirmation, :with => "testpassword"
        click_button 'Sign up'
      end

      it { status_code.should == 200 }
      # it { should have_content("Success") }
    end
  end
end

describe "when user visits homepage" do
  subject { page }
  describe "and the user is logged out" do
    before { visit root_url }
    it { status_code.should == 200 }
    it { should have_content("Read everything by your favorite writers") }
  end

  describe "and the user is logged in" do
    before do
      user = FactoryGirl.create(:user, display_name: "TestUserDisplayName")
      visit new_user_session_path
      fill_in :user_email, :with => "testuser@example.com"
      fill_in :user_password, :with => "randompassword"
      click_on "Sign in"
    end

    it { status_code.should == 200 }
    it { should have_content("TestUserDisplayName") }
    
    describe "should not be redirected" do
      before { visit root_url }
      it { status_code.should == 200 }
      # it { should have_selector("html.landing") }
      it { should have_content("Read everything by your favorite writers") }
    end
  end
end 


