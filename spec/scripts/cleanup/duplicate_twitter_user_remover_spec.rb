require 'spec_helper'

describe Cleanup::DuplicateTwitterUserRemover do
  describe "When cleaning up duplicate data" do

    before do
      3.times { FactoryGirl.create(:twitter_user, screen_name: "test_user_1", user_id: 1234) }
      7.times { FactoryGirl.create(:twitter_user, screen_name: "test_user_2", user_id: 2345) }
    end

    it "should not affect if screen_name not found" do
      TwitterUser.all.count.should be(10)
      remover = Cleanup::DuplicateTwitterUserRemover.new("unknown_user")
      remover.remove
      TwitterUser.all.count.should be(10)
    end

    it "should only remove given screen_name user" do
      remover = Cleanup::DuplicateTwitterUserRemover.new("test_user_1")
      remover.remove
      TwitterUser.all.count.should be(8)
      TwitterUser.where(screen_name: "test_user_1").count.should be(1)
      TwitterUser.where(screen_name: "test_user_2").count.should be(7)
    end

    it "should work with wrong case of screen_name" do
      remover = Cleanup::DuplicateTwitterUserRemover.new("Test_USer_2")
      remover.remove
      TwitterUser.all.count.should be(4)
      TwitterUser.where(screen_name: "test_user_2").count.should be(1)
      TwitterUser.where(screen_name: "test_user_1").count.should be(3)
    end
  end
end