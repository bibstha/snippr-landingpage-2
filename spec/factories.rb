FactoryGirl.define do
  factory :user do
    display_name     "Test User"
    email    "testuser@example.com"
    password "randompassword"
    password_confirmation { |u| u.password }
  end

  factory :profile do
    name "Test Author"
    short_description "Dummy Author Description"
  end

  factory :feed do
    title "Feed Title"
    url "Feed URL"
  end

  factory :feed_item do
  end
end