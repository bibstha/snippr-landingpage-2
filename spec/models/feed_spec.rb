require 'spec_helper'

describe Feed do
  it "Crawl RSS for the first time should return 10 items" do
    FactoryGirl.create(:feed, feed_url: "blog.biancawelds.com/feed")
    feed = Feed.first

    FeedItem.count.should eq(0)
    feed.pull
    FeedItem.count.should eq(10)
    feed.etag.should_not eq("")
    feed.etag.should_not be_blank
    feed.etag.length.should > 10
    feed.feed_class.should eq("Feedzirra::Parser::RSS")
    feed.feed_item_class.should eq("Feedzirra::Parser::RSSEntry")
    feed.feed_url.should eq("http://blog.biancawelds.com/feed/")
  end

  it "Crawl RSS for new item for the first time should create the object and return 10 items" do
    feed = Feed.new
    feed.feed_url = "http://blog.biancawelds.com/feed"

    Feed.count.should eq(0)    
    FeedItem.count.should eq(0)
    feed.pull
    Feed.count.should eq(1)

    FeedItem.count.should eq(10)
    feed.etag.should_not eq("")
    feed.etag.length.should > 10
  end

  # Seems like no etag in this feed
  it "Crawl RSS for the second time should return 2 items" do
    FactoryGirl.create(:feed, feed_url: "http://localhost:5000/feed?unit=second&interval=4")
    feed = Feed.first
    feed.pull
    fzo = feed.feedzirra_object
    fzo.entries.count.should eq(10)
    Kernel.sleep(5.0)
    feed2 = Feed.first
    feed2.pull
    b = feed2.feedzirra_object
    b.updated?.should eq(true)
    b.new_entries.count.should  eq(1)
  end

  it "Crawl RSS for the second time should return 2 items" do
    FactoryGirl.create(:feed, feed_url: "blog.biancawelds.com/feed")
    feed = Feed.first
    feed.pull
    fzo = feed.feedzirra_object
    fzo.entries.count.should eq(10)
    Kernel.sleep(5.0)
    b = Feedzirra::Feed.update(fzo)
    b.updated?.should be(false)
  end

  describe "When pulling data for existing feed" do
    before do
      FactoryGirl.create(:feed, feed_url: "blog.biancawelds.com/feed")
      feed = Feed.first
      feed.pull
    end

    it "should return 2 new feeds" do
      FeedItem.count.should eq(10)
      feed = Feed.first
      feed.pull
      fzo = feed.feedzirra_object
      fzo.updated?.should eq(false)
      fzo.new_entries.should be_blank
    end
  end  

end
