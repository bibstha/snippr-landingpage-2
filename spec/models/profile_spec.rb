require 'spec_helper'

describe Profile do
  describe "When kale wants to run a module" do
    it "should run profile_ranking" do
      feed1 = FactoryGirl.create(:feed, last_modified: "05/04/2013".to_date)
      feed2 = FactoryGirl.create(:feed, last_modified: "05/03/2013".to_date)
      feed3 = FactoryGirl.create(:feed, last_modified: "06/02/2013".to_date)

      feed_item1 = FactoryGirl.create(:feed_item, feed_token: feed1.token)
      feed_item2 = FactoryGirl.create(:feed_item, feed_token: feed2.token)
      profile = FactoryGirl.create(:profile, twitter_handle: "testuser", feed_tokens: [feed1.token, feed2.token, feed3.token])

      twitter_user = FactoryGirl.create(:twitter_user, screen_name: "testuser", followers_count: 1000, friends_count: 100)
      

      twitter_handle = "testuser"
      profile.update_profile_ranking(twitter_handle).should eq(717)
    end
  end

  describe "to generate the ranks" do
    it "should run generate profile ranking scores" do
      
      profile.generate_all_profile_ranking
    end
  end

end
