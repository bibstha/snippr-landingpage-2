require 'spec_helper'

describe User do
  before do
    @user = User.new(
      display_name: "test_name", 
      email: "test_name@example.com",
      password: "randompassword"
    )
  end

  subject { @user }

  it { should respond_to(:display_name) }
  it { should respond_to(:email) }
  it { should respond_to(:full_name) }
  it { should be_valid }

  # We leave the email, password validation to be done by
  # devise
  describe "when email is blank" do
    before { @user.email = "" }
    it { should be_invalid }
  end

  describe "when password is blank" do
    before { @user.password = "" }
    it { should be_invalid }
  end

  describe "when display_name is blank" do
    before { @user.display_name = "" }
    it { should be_invalid }
  end

  describe "when display_name is larger than 20 characters" do
    before { @user.display_name = "a" * 21 }
    it { should be_invalid }
  end

  describe "when display_name is less tha or equals to 20 characters" do
    before { @user.display_name = "a" * 20 }
    it { should be_valid }
  end  

  describe "when user saves the favorite article" do
    it "should add favorite article token to user favorite_tokens fields" do
      user = FactoryGirl.create(:user, display_name: "test_name", email: "test_name@example.com", password: "randompassword")
      user.save_favorite_articles('vvBt')

      user.favorite_articles.count.should eq(1)
      user.favorite_articles[0][:feed_token].should eq('vvBt')
    end
  end
end
