require 'spec_helper'

describe TwitterUser do
  describe "Filtering existing twitter users" do
    let(:twitter_user) do
      FactoryGirl.create(:twitter_user, user_id: 2, screen_name: "TwitterUser")
    end

    it do
      twitter_user.user_id # to make sure object is loaded
      TwitterUser.all.count.should eq(1)
      TwitterUser.all.first.should eq(twitter_user)
      TwitterUser.user_exists?(2).should eq(twitter_user)
      TwitterUser.user_exists?(1).should eq(nil)
      TwitterUser.user_exists?("RandomUser", true).should eq(nil)
      TwitterUser.user_exists?("TwitterUser", true).should eq(twitter_user)
      TwitterUser.user_exists?("twitterUSER", true).should eq(twitter_user)

      ids = [1,2,3]
      filtered_ids = ids.find_all { |id| TwitterUser.user_exists? id, false }
      filtered_ids.should eq([2])

      ids = ["usera", "userb", "TwitterUser", "twitterUSER"]
      filtered_ids = ids.find_all { |id| TwitterUser.user_exists? id, true }
      filtered_ids.should eq(["TwitterUser", "twitterUSER"])
    end
    it do 
      TwitterUser.all.count.should eq(0)
      TwitterUser.user_exists?(2).should eq(nil)
    end
  end

  describe "When checking if TwitterUser has been crawled" do
    it do
      user1 = FactoryGirl.create(:twitter_user, user_id: 1, screen_name: "TwitterUser1", users_lookup_done: true)
      user2 = FactoryGirl.create(:twitter_user, user_id: 2, screen_name: "TwitterUser2")
      TwitterUser.users_lookup_done?(1).should eq(true)
      TwitterUser.users_lookup_done?(2).should eq(false)
      TwitterUser.users_lookup_done?("TwitterUser1", true).should eq(true)
      TwitterUser.users_lookup_done?("TwitterUser2", true).should eq(false)
      TwitterUser.users_lookup_done?("TWITTERUSER1", true).should eq(true)
      TwitterUser.users_lookup_done?("TWITTERUSER2", true).should eq(false)
    end
  end

  describe "When Extracting Data from Twitter" do
    it do
      TwitterUser.user_exists?("bimalmaharjan", use_screen_name=true).should eq(nil)
      TwitterUser.pull_users_lookup "bimalmaharjan", true
      TwitterUser.all.count.should eq(1)
      TwitterUser.user_exists?("bimalmaharjan", true).user_id.should eq(89497237)
      TwitterUser.users_lookup_done?("bimalmaharjan", true).should eq(true)
      TwitterUser.users_lookup_done?("BimalMaharjan", true).should eq(true)
      TwitterUser.all.first.user_id.should eq(89497237)
    end

    it do
      TwitterUser.pull_users_lookup "bimalmaharjan,bibstha", true
      TwitterUser.all.count.should eq(2)
      TwitterUser.user_exists?("bimalmaharjan", true).user_id.should eq(89497237)
      TwitterUser.users_lookup_done?("bimalmaharjan", true).should eq(true)
      TwitterUser.user_exists?("bibstha", true).user_id.should eq(17726589)
      TwitterUser.users_lookup_done?("bibstha", true).should eq(true)
      user_bibstha = TwitterUser.find_by(screen_name: "bibstha")
      user_bibstha.friends_count.should be > 100
      user_bibstha.followers_count.should be > 100
      user_bibstha.name.should eq("Bibek Shrestha")
      user_bibstha.location.should eq("Kathmandu, Nepal")
      user_bibstha.expanded_url.should eq(nil)
    end

    it do
      TwitterUser.pull_users_lookup "BimalMaharjan,BiBstha", true
      TwitterUser.all.count.should eq(2)
      TwitterUser.user_exists?("bimalmaharjan", true).user_id.should eq(89497237)
      TwitterUser.user_exists?("bibstha", true).user_id.should eq(17726589)
      TwitterUser.all.map{ |user| user.screen_name }.should eq(["bimalmaharjan", "bibstha"])
    end

    it do
      TwitterUser.pull_users_lookup "89497237,17726589"
      TwitterUser.all.count.should eq(2)
      TwitterUser.user_exists?("bimalmaharjan", true).user_id.should eq(89497237)
      TwitterUser.users_lookup_done?("bimalmaharjan", true).should eq(true)
      TwitterUser.user_exists?("bibstha", true).user_id.should eq(17726589)
      TwitterUser.users_lookup_done?("bibstha", true).should eq(true)
    end

    it "should not pull the same data twice" do
      TwitterUser.pull_users_lookup "89497237,17726589"
      TwitterUser.count.should eq(2)

      TwitterUser.pull_users_lookup "89497237,17726589"
      TwitterUser.count.should eq(2) # still only two as they were crawled earlier
    end
  end

  describe "When converting Twitter JSON to TwitterUser Object" do
    it "valid json string should give proper object" do
      json_string = '{"id":9876,"id_str":"9876","name":"test user","screen_name":"testuser","location":"from  Nepal.","description":"co-founder http:\/\/t.co\/3gbeRJPcpG , currently at http:\/\/t.co\/xhYXzp6fYV , Trento, Italy.","url":null,"entities":{"description":{"urls":[{"url":"http:\/\/t.co\/3gbeRJPcpG","expanded_url":"http:\/\/www.snippr.co","display_url":"snippr.co","indices":[11,33]},{"url":"http:\/\/t.co\/xhYXzp6fYV","expanded_url":"http:\/\/www.techpeaks.eu","display_url":"techpeaks.eu","indices":[49,71]}]}},"protected":false,"followers_count":200,"friends_count":206,"listed_count":7,"created_at":"Thu Nov 12 17:43:37 +0000 2009","favourites_count":1,"utc_offset":20700,"time_zone":"Kathmandu","geo_enabled":true,"verified":false,"statuses_count":1058,"lang":"en","status":{"created_at":"Fri Oct 04 11:07:36 +0000 2013","id":386085187471159296,"id_str":"386085187471159296","text":"@bibstha says \"entire code is ready and it is executing in my mind.i m just bored to type it\"","source":"web","truncated":false,"in_reply_to_status_id":null,"in_reply_to_status_id_str":null,"in_reply_to_user_id":17726589,"in_reply_to_user_id_str":"17726589","in_reply_to_screen_name":"bibstha","geo":null,"coordinates":null,"place":null,"contributors":null,"retweet_count":0,"favorite_count":0,"entities":{"hashtags":[],"symbols":[],"urls":[],"user_mentions":[{"screen_name":"bibstha","name":"Bibek Shrestha","id":17726589,"id_str":"17726589","indices":[0,8]}]},"favorited":false,"retweeted":false,"lang":"en"},"contributors_enabled":false,"is_translator":false,"profile_background_color":"C0DEED","profile_background_image_url":"http:\/\/abs.twimg.com\/images\/themes\/theme1\/bg.png","profile_background_image_url_https":"https:\/\/abs.twimg.com\/images\/themes\/theme1\/bg.png","profile_background_tile":false,"profile_image_url":"http:\/\/a0.twimg.com\/profile_images\/1826494056\/Bimal_normal.jpg","profile_image_url_https":"https:\/\/si0.twimg.com\/profile_images\/1826494056\/Bimal_normal.jpg","profile_banner_url":"https:\/\/pbs.twimg.com\/profile_banners\/89497237\/1380050850","profile_link_color":"0084B4","profile_sidebar_border_color":"C0DEED","profile_sidebar_fill_color":"DDEEF6","profile_text_color":"333333","profile_use_background_image":true,"default_profile":true,"default_profile_image":false,"following":true,"follow_request_sent":false,"notifications":false}'
      api_json_string = "[#{json_string}]"
      twitter_users = TwitterUser.from_json api_json_string
      twitter_users.count.should eq(1)
      twitter_users[0].user_id.should eq(9876)
      twitter_users[0].screen_name.should eq("testuser")
      twitter_users[0].users_lookup_json_data.should eq(JSON.generate(JSON.parse(json_string)))
    end

    it "invalid json string should give nil" do
      json_string = '[{"error": "some random string"}]'
      twitter_users = TwitterUser.from_json json_string
      twitter_users.should eq(nil)
    end
  end

  describe "When extracting user list for a user" do
    it "should check if users already have friends_ids_done" do
      FactoryGirl.create(:twitter_user, user_id: 1, screen_name: "bimal", friends_ids_done: true, friends_ids: [1234])
      FactoryGirl.create(:twitter_user, user_id: 2, screen_name: "newbimal", friends_ids: [1234])
      
      TwitterUser.friends_ids_done?(1).should eq(true)
      TwitterUser.friends_ids_done?(10).should eq(nil)
      TwitterUser.friends_ids_done?("bimal", true).should eq(true)
      TwitterUser.friends_ids_done?("haku").should eq(nil)

      TwitterUser.friends_ids_done?(2).should eq(nil)
      TwitterUser.friends_ids_done?("newbimal", true).should eq(nil)
    end

    it "should reject users that are already crawled" do
      tu = FactoryGirl.create(:twitter_user, user_id: 1, screen_name: "bimal", friends_ids_done: true, friends_ids: [1234])
      TwitterUser.pull_friends_ids("bimal", true)
      TwitterUser.all.count.should eq(1) # new user shouldn't be creaed, we already have one
      TwitterUser.all.first.friends_ids.should eq([1234])

      TwitterUser.pull_users_lookup("bimalmaharjan", true)
      TwitterUser.pull_friends_ids("bimalmaharjan", true)
      TwitterUser.all.count.should eq(2)
      tu2 = TwitterUser.find_by(screen_name: "bimalmaharjan")
      tu2.friends_ids_done.should eq(true)
      tu2.friends_ids.count.should be > 10
    end

    it "should pull users" do
    end
  end

  describe "When checking for valid url" do
    it do 
      twitter_user = FactoryGirl.create(:twitter_user, feed_links_parse_done: true, feed_links: [
        {"title" => "Feed Example 1", "href" => "http://example.org/1.rss"},
        {"title" => "Feed Example 1", "href" => "http://example.org/1.rss"},
        {"title" => "Feed Example 1 Different Title But Same Href", "href" => "http://example.org/1.rss"},
        {"title" => "Feed Example 2", "href" => "http://example.org/1.atom"},
        {"title" => "Feed Example 2", "href" => "http://example.org/1.atom"},
        {"title" => "Feed Example 2", "href" => "http://example.org/1.atom"},
        {"title" => "Passion Capital » TEST Feed", "href" => "http://passioncapital.com/comments/feed/"},
        {"title" => "Passion Capital » COMMENT Feed", "href" => "http://passioncapital.com/feed/"},
        {"title" => "Passion Capital » Atom Feed", "href" => "http://passioncapital.com/comments/feed/"},
        {"title" => "Passion Capital » TEST Feed", "href" => "http://passioncapital.com/comments/feed/atom/"}
      ])

      twitter_user.filtered_feeds.should eq([{"title" => "Feed Example 1", "href" => "http://example.org/1.rss"}])
    end
  end
end
