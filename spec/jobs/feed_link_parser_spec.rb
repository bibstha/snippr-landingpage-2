require 'spec_helper'

describe FeedLinkParser do
  describe "When Crawling Link" do
    
    let(:parser) { FeedLinkParser.new("http://blog.biancawelds.com/") }

    it "should be able to get the body" do  
      parser.parse
      response = parser.response
      response.code.should eq 200
    end

    it "should return feed links" do
      parser.parse
      feed_links = parser.feed_links
      feed_links.count.should eq(2)
      feed_links[0].should eq({title: "The life and times of Bianca RSS Feed", href: "http://blog.biancawelds.com/feed/"})
      feed_links[1].should eq({title: "The life and times of Bianca Atom Feed", href: "http://blog.biancawelds.com/feed/atom/"})
    end
  end
end
