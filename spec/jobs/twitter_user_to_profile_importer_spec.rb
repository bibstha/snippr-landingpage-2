require 'spec_helper'

describe TwitterUserToProfileImporter do
  describe "When importing user" do
    it "validating feed_links" do
      twitter_user = FactoryGirl.create(:twitter_user, feed_links_parse_done: true, feed_links: [
        {"title" => "Feed Example 1", "href" => "http://example.org/1.rss"},
        {"title" => "Feed Example 2", "href" => "http://example.org/1.atom"}
      ])

      importer = TwitterUserToProfileImporter.new("twitter_user")
      filtered_feed_links = importer.filter_feed_links(twitter_user.feed_links)
      filtered_feed_links.should eq([
        {"title" => "Feed Example 1", "href" => "http://example.org/1.rss"},
      ])
    end

    it "validating feed_links" do
      twitter_user = FactoryGirl.create(:twitter_user, feed_links_parse_done: true, feed_links: [
        {"title" => "Feed Example 1", "href" => "http://example.org/1.rss"},
        {"title" => "Feed Example 2", "href" => "http://example.org/1.atom"},
        {"title" => "Passion Capital » TEST Feed", "href" => "http://passioncapital.com/comments/feed/"},
        {"title" => "Passion Capital » COMMENT Feed", "href" => "http://passioncapital.com/feed/"},
        {"title" => "Passion Capital » Atom Feed", "href" => "http://passioncapital.com/comments/feed/"},
        {"title" => "Passion Capital » TEST Feed", "href" => "http://passioncapital.com/comments/feed/atom/"}
      ])

      importer = TwitterUserToProfileImporter.new("twitter_user")
      filtered_feed_links = importer.filter_feed_links(twitter_user.feed_links)
      filtered_feed_links.should eq([{"title" => "Feed Example 1", "href" => "http://example.org/1.rss"}])
    end

    it "should create profile if doesn't exist already" do
      twitter_user = FactoryGirl.create(:twitter_user, screen_name: "twitter_user", feed_links_parse_done: true, feed_links: [
        {title: "Feed Example 1", href: "http://example.org/1.rss"},
        {title: "Feed Example 2", href: "http://example.org/2.rss"}
      ], name: "Twitter User")
      
      Profile.all.count.should eq(0)
      Feed.all.count.should eq(0)

      importer = TwitterUserToProfileImporter.new("twitter_user")
      importer.import
      
      Profile.all.count.should eq(1)
      profile = Profile.first
      profile.twitter_handle.should eq("twitter_user")

      Feed.all.count.should eq(2)
      feed1 = Feed.find_by(feed_url: "http://example.org/1.rss")
      feed1.title.should eq("Feed Example 1")
      feed2 = Feed.find_by(feed_url: "http://example.org/2.rss")
      feed2.title.should eq("Feed Example 2")

      profile.feed_tokens.should eq([feed1.token, feed2.token])
    end

    it "should create Feed instances only for those that do not exist" do
      twitter_user = FactoryGirl.create(:twitter_user, screen_name: "twitter_user", feed_links_parse_done: true, feed_links: [
        {title: "Feed Example 1", href: "http://example.org/1.rss"},
        {title: "Feed Example 2", href: "http://example.org/2.rss"}
      ], name: "Twitter User")
      
      FactoryGirl.create(:feed, title: "Feed Example ABCD", feed_url: "http://example.org/1.rss")

      Feed.all.count.should eq(1)
      
      importer = TwitterUserToProfileImporter.new("twitter_user")
      importer.import

      Feed.all.count.should eq(2)
      feed1 = Feed.find_by(feed_url: "http://example.org/1.rss")
      feed1.title.should eq("Feed Example ABCD")
      feed2 = Feed.find_by(feed_url: "http://example.org/2.rss")
      feed2.title.should eq("Feed Example 2")

      Profile.find_by(twitter_handle: "twitter_user").feed_tokens.should eq([feed1.token, feed2.token])
    end

    it "should check if number of words is exactly equal to 2" do
      t = FactoryGirl.create(:twitter_user, name: "a")
      TwitterUserToProfileImporter.condition(t).should be(false)

      t.name = "a b"
      TwitterUserToProfileImporter.condition(t).should be(true)

      t.name = "a b c"
      TwitterUserToProfileImporter.condition(t).should be(false)

      t.name = "a b  "
      TwitterUserToProfileImporter.condition(t).should be(true)
    end
  end  
end