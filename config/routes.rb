Landingpage::Application.routes.draw do
  resources :feed_items do
    resources :comments
  end

  devise_for :users
  
  authenticated :user do
    # root :to => "feed#index" #timeline
    root :to => "feed#timeline"
  end
  unauthenticated :user do
    devise_scope :user do 
      get "/" => "devise/registrations#new"
    end
  end

  get "welcome", to: redirect("/users/sign_in")
  get "logout" => "page#logout"

  #first time log in
  get "users/:id/first_login" => "feed#user_first_login", as: "user_first_login"

  get "users/:id/feeds" => "feed#user_news_feed", as: "user_news_feed"
  post "users/readlists" => "feed#user_readlist_create"
  get "users/readlists" => "feed#user_readlists", as: "user_readlists"
  get "users/:user_token/readlists" => "feed#user_readlists"
  get "users/readlist/:token" => "feed#user_readlist_read", as: "user_readlist_read"
  get "users/:user_token/following_profiles" => "feed#user_following_profiles"
  delete "users/readlist/:token" => "feed#user_readlist_delete", as: "user_readlist_delete"
  post "readlist/:token/privacy" => "feed#user_readlist_is_public", as: "user_readlist_is_public"

  # Author Routes
  get "timeline" => "feed#timeline", as: "timeline"
  get "authors" => "feed#author_list", as: "author_list"
  get "recommendedauthors" => "feed#author_recommendation", as: "author_recommendation"
  get "authors/:author_token" => "feed#author_show_posts", as: "author_show_posts"
  get "search/authors" => "feed#author_search", as: "search_author"
  post "authors/:token/follow" => "feed#author_follow", as: "author_follow"

  
   
  post "subscribe" => "page#homepage_subscribe"
  get "survey/2" => "page#survey_2"
  get "admin/subscriptions" => "page#admin_subscriptions"


  get "admin/authors" => "profile#index"
  get "admin/posts" => "profile#admin_posts"
  post "admin/authors" => "profile#create"
  put "admin/authors/:token" => "profile#update"
  get "admin/authors/:token" => "profile#show", as: "admin_author_show"
  delete "admin/authors/:token" => "profile#delete"
  delete "admin/authors/:token/feed/:feed_token" => "profile#dissociate_feed"
  post "admin/authors/feeds/:author_token" => "profile#feed_update"

  get "admin/feeds" => "admins/feeds#feed"

  get "admin/authorsearchlist" => "profile#author_search_list"
  get "page/page2"


  get "post/:token" => "feed#post_view", as: 'post_view'

  get "post/1/:token" => "feed#post_view1"
  get "post/2/:token" => "feed#post_view2"
  get "post/3/:token" => "feed#post_view3"
  get "post/4/:token" => "feed#post_view4"
  get "post/5/:token" => "feed#post_view5", as: "post_view5"
  get "post/6/:token" => "feed#post_view6"

  post "like" => "likes#toggle"
  get "likes/:type/:type_token" => "likes#index"

  # get "/feed_items/:feed_item_id/comments" => "comments#index", as: "feed_item_comments"
  # post "/feed_items/:feed_item_id/comments" => "comments#create"
end
