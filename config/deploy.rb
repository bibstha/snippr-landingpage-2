require 'bundler/capistrano'
require 'rvm/capistrano'
# load 'lib/deploy/seed'

set :rvm_ruby_string, :local              # use the same ruby as used locally for deployment
set :rvm_autolibs_flag, "read-only"       # more info: rvm help autolibs
before 'deploy:setup', 'rvm:install_rvm'  # install RVM
before 'deploy:setup', 'rvm:install_ruby' # install Ruby and create gemset, OR:

set :application, "Snippr Main"
set :repository,  "git@bitbucket.org:bibstha/snippr-landingpage-2.git"

# set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

set :scm, "git"
set :user, "www-data"
set :branch, "master"
set :deploy_via, :remote_cache
set :deploy_to, "/var/www/snippr.co/landing"
set :use_sudo, false

role :web, "88yaks.com"                          # Your HTTP server, Apache/etc
role :app, "88yaks.com"                          # This may be the same as your `Web` server
role :db,  "88yaks.com", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"

# if you want to clean up old releases on each deploy uncomment this:
after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end

## IMPROTANT THIGNS TO NOTE
# When running resque, set rails_env in production

set :whenever_command, "bundle exec whenever"
set :whenever_identifier, defer { "#{:snippr}_#{:production}" }
require "whenever/capistrano"
after "deploy:restart", "whenever:update_crontab"


# Synchronize mongo databases
set :mongodbname_prod, 'landingpage_production'
set :mongodbname_dev, 'landingpage_development'

namespace :sync do
  desc 'Synchronize local MongoDB with production.'
  task :mongodb, :roles => "#{:db}" do
    run "cd"
    run "mongodump --host localhost -d #{mongodbname_prod}"
    current_host = capture("echo $CAPISTRANO:HOST$").strip
    system "scp -Cr #{user}@#{current_host}:~/dump/#{mongodbname_prod}/ db/dumps/"
    system "mongorestore -h localhost --drop -d #{mongodbname_dev} db/dumps/#{mongodbname_prod}"
  end
end

# Resque
after "deploy:restart", "deploy:restart_workers"
def run_remote_rake(rake_cmd)
  rake_args = ENV['RAKE_ARGS'].to_s.split(',')
 
  cmd = "cd #{fetch(:latest_release)} && bundle exec #{fetch(:rake, "rake")} RAILS_ENV=#{fetch(:rails_env, "production")} #{rake_cmd}"
  cmd += "['#{rake_args.join("','")}']" unless rake_args.empty?
  run cmd
  set :rakefile, nil if exists?(:rakefile)
end
 
namespace :deploy do
  desc "Restart Resque Workers"
  task :restart_workers, :roles => :web do
    run_remote_rake "resque:restart_workers"
  end
end