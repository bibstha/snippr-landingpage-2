class Cleanup::DuplicateTwitterUserRemover
  def initialize(screen_name)
    @screen_name = screen_name
  end

  def remove
    users = TwitterUser.where(screen_name: /^#{@screen_name}$/i)
    not users.blank? and users.count > 1 and Proc.new do
      TwitterUser.where(screen_name: /^#{@screen_name}$/i, "id" => {"$ne" => users.first.id}).delete
    end.call
  end
end

# Call Remover
if __FILE__ == $0
  TwitterUser.distinct(:screen_name).each do |screen_name|
    Cleanup::DuplicateTwitterUserRemover.new(screen_name).remove
  end
end
