class LikesController < ApplicationController
  # POST /like
  def toggle
    authenticate_user!
    if params[:object_type].blank? or params[:object_token].blank?
      return render json: {status: 'error', message: 'object_type and object_token parameters cannot be empty'},
        status: 400
    end

    object_type = params[:object_type]
    object_token = params[:object_token]

    # Only supports feed_item
    unless object_type == "feed_item"
      return render json: {status: 'error', message: 'invalid object_type'}, status: 400
    end

    feed_item = FeedItem.find(object_token)
    unless feed_item
      return render json: {status: 'error', message: 'invalid object_token'}, status: 400
    end

    status = Like.toggle('feed_item', object_token, current_user.token)
    if status
      return render json: {status: 'ok', message: 'Like created', value: true}
    else
      return render json: {status: 'ok', message: 'Like deleted', value: false}
    end
  end

  # GET /like/type/token
  def index
    authenticate_user!
    type = params[:type]
    type_token = params[:type_token]
    if Like.find_by(type: type, type_token: type_token, user_token: current_user.token)
      return render json: {value: true}
    else
      return render json: {value: false}
    end
  end
end
