class FeedController < ApplicationController  

  def author_list
    authenticate_user!
    @orgs = {
      'standford-news-insights' => 'Stanford',
      'insead-blog' => 'INSEAD',
      'havard-business-school' => 'Havard',
      'dartmouth-newsroom' => 'Dartmouth',
      'columbia-global-insights' => 'Chicago',
      'kellog-blogs' => 'Kellog',
      'imd-tomorrows-challenges' => 'IMD',
    }
    # @profiles = Profile.where("token" => {"$in"=> current_user.following_profiles})
    @disable_search_nav = true
    if request.xhr?
      page = params[:page] ? params[:page] : 1
      org = params[:org]
      if org
        @profiles = Profile.where("tags" => org).order_by(:name => 1).page(page)
        render template: "feed/author_list.json", content_type: "application/json"
      else
        @profiles = Profile.order_by(:name => 1).page(page)
        render template: "feed/author_list.json", content_type: "application/json"
      end
    end
  end

  def author_show_posts
    authenticate_user!
    
    paginate_page = (params[:page].blank?) ? 0 : params[:page]

    @author_token = params[:author_token]

    current_user.following_profiles ||= []
    @following_authors = Profile.where("token" => {"$in"=> current_user.following_profiles})
    @author = Profile.find(@author_token);
    @author.feed_tokens ||= []
    
    @feed_items = FeedItem.or({"feed_token" => {"$in" => @author.feed_tokens}}, {"profile_tokens" => @author.token}).
      order_by("published desc").page(paginate_page)
    @paginate_count = FeedItem.or({"feed_token" => {"$in" => @author.feed_tokens}}, {"profile_tokens" => @author.token}).count
  end

  def author_search
    search_condition =  params[:search]
    if not search_condition.blank?    
      @author_search_result = Profile.author_search(search_condition,current_user.email)
    end
    @disable_search_nav = true
  end

  def author_follow
    # authenticate_user!
    # if request.xhr?
      # @todo check valid user
      token = params[:token]
      # unless Profile.find(token)
      #   return render json: {status: "error", message: "Invalid token."}
      # end

      added = true

      current_user.following_profiles ||= []
      if current_user.following_profiles.include?(token)
        current_user.following_profiles.delete token
        added = false
      else
        current_user.following_profiles << token
      end
      current_user.save

      if added
        render json: { status: "ok", message: "following user", value: added }
      else
        render json: { status: "ok", message: "unfollowing user", value: added }
      end
  end


  def user_news_feed
    authenticate_user!
    
    if current_user.first_time
      return redirect_to author_list_path    
    end   

    # pagination components
    default_limit = 20
    default_page = 1
    paginate_limit = (params[:limit].blank?) ? default_limit : params[:limit]
    paginate_page = (params[:page].blank?) ? default_page : params[:page]    
      
    @following_authors = Profile.where("token" => {"$in"=> current_user.following_profiles})
    feed_tokens = []
    
    @following_authors.each do |following_author|
      feed_tokens += following_author.feed_tokens  if following_author.feed_tokens.kind_of?(Array)
    end   

    @feed_items = FeedItem.where("feed_token" => {"$in" => feed_tokens}).
        order_by("published desc").page(paginate_page)

    @paginate_count = FeedItem.where("feed_token" => {"$in" => feed_tokens}).count
  end

  def post_view
    authenticate_user!
    token = params[:token]
    @feed_item = FeedItem.find(token)
    not_found unless @feed_item
    
    if @feed_item
      user_token = 'anonymous'
      user_token = current_user.token if user_signed_in?
      ModelViewLog.create(type: 'feed_item', type_token: @feed_item.token, user_token: user_token)
      @feed_item.update_attributes(view_count: @feed_item.view_count + 1)
    end

    @author = Profile.find_by("feed_tokens" => @feed_item.feed_token) 
    unless @author
      @author = Profile.find(@feed_item.profile_tokens[0])
    end

    @model = @feed_item
    @comment = Comment.new
    @comment.author = current_user.token
  end

  def post_view1
    token = params[:token]
    @feed_item = FeedItem.find(token)
  end

  def post_view2
    token = params[:token]
    @feed_item = FeedItem.find(token)
  end
  def post_view3
    token = params[:token]
    @feed_item = FeedItem.find(token)
    render layout: "signup"
  end
  def post_view4
    token = params[:token]
    @feed_item = FeedItem.find(token)
    render layout: "signup"
  end
  def post_view5
    authenticate_user!
    @profiles = Profile.all.sort({:score => -1}).limit(3)
    @feed_items = FeedItem.all.paginate(limit: 25)

    @search_condition =  params[:search]
    if not @search_condition.blank?    
      @author_search_result = Profile.author_search(@search_condition,current_user.email)
    end
  end

  def author_recommendation
    authenticate_user!
    recommend_author = Profile.new
    @profiles = recommend_author.author_recommend(current_user.token)
    @disable_search_nav = true
  end

  def index
    authenticate_user!
    # return redirect_to user_news_feed_path(current_user.token)
    return redirect_to timeline_path
  end

  # GET /timeline
  def timeline
    authenticate_user!
    if request.xhr?
      page = params[:page] ? params[:page] : 1
      @feed_items = FeedItem.order_by(published: -1).page(page)
      return render template: "feed/timeline.json", content_type: "application/json"
    end
  end

  # POST /users/readlist
  def user_readlist_create
    authenticate_user!

    readlist_name = params[:title]
    if readlist_name.blank?
      return render json: {status: "error", message: "Readlist cannot be empty"}
    end
    
    profile_token = params[:profile_token]
    profile = Profile.find(profile_token)
    if profile.blank?
      return render json: {status: "error", message: "Profile not found"}
    end

    current_user.following_profiles ||= []
    if not current_user.following_profile? profile.token
      current_user.following_profiles << profile_token unless current_user.following_profile?(profile.token)
      current_user.save
    end

    current_user.readlist_tokens ||= []
    readlist = Readlist.find_by(title: readlist_name, token: {"$in" => current_user.readlist_tokens}) 
    unless readlist
      readlist = Readlist.create(title: readlist_name, profile_tokens: [profile_token])      
      current_user.readlist_tokens << readlist.token
      current_user.save
      return render json: {status: "ok", message: "Readlist created and Author added to Readlist", action: "add"}
    end

    readlist.profile_tokens ||= []
    if readlist.profile_tokens.include? profile_token
      readlist.profile_tokens.delete profile_token
      readlist.save
      return render json: {status: "ok", message: "Author removed from existing Readlist", action: "delete"}
    else
      readlist.profile_tokens << profile_token
      readlist.save
      return render json: {status: "ok", message: "Author added to existing Readlist", action: "add"}
    end
  end

  # DELETE /users/readlist/:token
  def user_readlist_delete
    authenticate_user!
    readlist = Readlist.find(params[:token])
    if current_user.readlist_tokens.include?params[:token]
      readlist.destroy
      current_user.readlist_tokens.delete params[:token]
      current_user.save
    end
    return redirect_to user_readlists_path
  end

  # Make readlist public or private /users/read

  def user_readlist_is_public
    authenticate_user!
    readlist = Readlist.find(params[:token])
    if current_user.readlist_tokens.include?params[:token]
      if readlist.is_public
        readlist.update(:is_public => false)
        is_public = false
        return render json: {status: "ok", message: "Readlist made private", value: is_public }
        #current_user.readlist_tokens.update(:is_public =>false) params[:token]
        
      else
        readlist.update(:is_public => true)
        is_public = true
        #current_user.readlist_tokens.update(:is_public =>true) params[:token]
         return render json: {status: "ok", message: "Readlist made public", value: is_public}
      end
      current_user.save
    end
    return redirect_to user_readlists_path
  end

  # GET /users/readlist
  # GET /users/:user_token/readlists
  def user_readlists
    authenticate_user!
    if request.xhr?
      readlists = {}
      current_user.my_readlists.each do |readlist|
        readlists[readlist.token] = readlist.as_json(except: "_id")
      end
      return render :json => { readlists: readlists }
    else
    end
  end

  # GET /user/readlist/:token
  def user_readlist_read
    authenticate_user!
    @readlist = Readlist.find(params[:token])
    @readlist.profile_tokens ||= []
    feed_tokens = []
    @readlist_authors = Profile.where(token: {"$in" => @readlist.profile_tokens})
      # .order_by("name")
    @readlist_authors.each do |profile|
      feed_tokens += profile.feed_tokens unless profile.feed_tokens.blank?
    end

    params[:per_page] ||= 20
    @feed_items = FeedItem.or({:feed_token.in => feed_tokens}, {:profile_tokens.in => @readlist.profile_tokens})
      .order_by("published desc")
      .page(params[:page])

    current_user.following_profiles ||= []
    @following_authors = Profile.where(token: {"$in" => current_user.following_profiles})
  end

  # GET /users/:user_token/following_profiles
  def user_following_profiles
    authenticate_user!
    if current_user.token != params[:user_token]
      # @todo check for permission to access other profiles
      # user = User.find(params[:user_token])
      return render json: {status: "error", message: "Access denied"}
    end
    
    if request.xhr?
      following_profiles = {}
      current_user.my_following_profiles.each do |p|
        following_profiles[p.token] = p.as_json(only: [:name, :token])
      end
      return render json: {following_profiles: following_profiles}
    end
  end
end


