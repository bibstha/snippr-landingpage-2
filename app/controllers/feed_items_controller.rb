class FeedItemsController < ApplicationController
  # This is put only to satisfy mongoid_commentable redirect issue
  def show
    return redirect_to post_view_path(params[:id])
  end
end
