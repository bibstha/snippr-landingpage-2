class Admins::FeedsController < ApplicationController
  def feed
    if params[:q]
      @feeds = Feed.where(feed_url: /#{params[:q]}/i).limit(50)
    else
      @feeds = []
    end
  end
end