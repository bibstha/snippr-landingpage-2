class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :configure_permitted_parameters, if: :devise_controller?
  before_action :record_action_log

  def quick_authenticate
    authenticate_or_request_with_http_basic do |username, password|
      username == "admin" && password == "Snippr88!"
    end
  end

  def after_sign_in_path_for(resource)
    if current_user.first_time
      author_list_path    
    else
      timeline_path
    end    
    # user_news_feed_path(id: current_user.token)
  end

  def after_sign_out_path_for(resource)
    # user_news_feed_path(id: current_user.token)
    new_user_session_path
  end

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :display_name
  end

  def record_action_log
    logging_params = params.select do |param|
      [:action, :target, :version].include? param
    end
    action_log = ActionLog.new
    action_log.action = logging_params[:action]
    action_log.target = logging_params[:target] || request.original_url
    action_log.version = logging_params[:version]
    action_log.ip = request.remote_ip
    action_log.referrer = request.referer
    action_log.save
  end
end
