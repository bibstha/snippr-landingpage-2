class PageController < ApplicationController
  def homepage
    render layout: "application.landing"
  end

  # post /subscribe
  def homepage_subscribe
    if params["email"] == ""
      data = { status: "fail"}
    else
      subscription = Subscription.create({
        email: params[:email],
        following_authors: params[:following],
        suggested_authors: params[:suggested_authors],
        created_date: Time.now
      });
      data = { status: "success" }
    end
    render :json => data
  end

  def survey_2
    render :layout => false
  end

  def admin_subscriptions
    @subscriptions = Subscription.all
    render layout: "application.landing"
  end

  def page2
  end

  def logout
    redirect_to destroy_user_session_path, method: "delete"
  end
end
