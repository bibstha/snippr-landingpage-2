class ProfileController < ApplicationController
  layout "admin"
  
  def index
    quick_authenticate
    if request.xhr?
      profiles = Profile.all
      output = []
      profiles.each do |profile|
        output << {
          name: profile.name,
          click_url: url_for(admin_author_show_path(profile.token)),
          token: profile.token
        }
      end
      render :json => output
    end
  end

  def admin_posts
    quick_authenticate
    page = params[:page] || 20
    per_page  = params[:per_page] || 50
    @feed_items = FeedItem.order_by('published desc').paginate(page: page, per_page: per_page)
  end

  def admin_authors
  end

  def show
    quick_authenticate
    @profile = Profile.find(params[:token])
    unless @profile
      return render :status => 400, :json => {status: "error", message: "Profile not found"}
    end

    @feeds = Feed.where(:token => {"$in" => @profile.feed_tokens}) if @profile.feed_tokens
    if request.xhr?
      render :json => {
        profile: @profile.as_json(:except => [:feed_tokens, "_id"]),
        feeds: @feeds.blank? ? [] : @feeds
      }
    end
  end

  def create
    quick_authenticate
    if request.xhr?
      profile = Profile.create(profile_params)
      render json: {profile: profile, feeds: []}
    end
  end

  def update
    quick_authenticate
    if request.xhr?
      token = params[:token]
      profile = Profile.find(token)
      if profile
        profile.update_attributes(profile_params)
        render :json => {status: "ok", message: "Profile Saved", data: profile_params}
      else
        render :json => {status: "invalid", message: "Profile Not Found"}
      end
    end
  end

  def delete
    quick_authenticate
    Profile.find(params[:token]).destroy;
    render :json => {:status => 'ok'}
  end

  def feed_update
    quick_authenticate
    if params[:feeds].blank?
      return render json: {status: "error", message: "Invalid data"}
    end

    author_token = params[:author_token]
    profile = Profile.find(author_token)
    unless profile
      return render :json => {status: "error", message: "Author not found."}
    end

    params[:feeds].each do |feed|
      if not feed["is_new"].blank? and feed["is_new"]
        if feed["feed_url"][0..3] != "http"
          feed["feed_url"] = "http://" + feed["feed_url"]
        end
        feed = Feed.find_by(feed_url: feed["feed_url"]) or Feed.create(feed.except! "is_new")
        if feed
          profile.feed_tokens ||= []
          profile.feed_tokens << feed.token
          profile.save!
          Resque.enqueue(FeedCrawler, feed.token)
        end
      end
    end
    render :json => {status: "ok", message: "Saves successfully!"}
  end

  #show the list of author searches

  def author_search_list
    quick_authenticate
    @author_search_list = UserAuthorSearch.all.order_by(:search_count => -1)      
    # if not @author_search_list.blank?
    #   render json: {author_list: @author_search_list}
    # else
    #   render json: {message: "No author searched"}
      
    # end

  end

  # DELETE /admin/authors/:token/feed/:feed_token
  def dissociate_feed
    quick_authenticate
    profile = Profile.find(params[:token])
    unless profile
      return render json: {status: "error", message: "Profile token not found"}
    end

    if profile.feed_tokens.is_a?(Array) and feed_token = profile.feed_tokens.delete(params[:feed_token])
      profile.save
      render json: {status: "ok", message: "#{params[:feed_token]} has been dissociated with profile #{params[:token]}."}
    else
      render json: {status: "error", message: "#{params[:feed_token]} not found."}
    end
  end

  private
  def profile_params
    return params.require(:profile).permit(:name, :short_description, :description, :photo_url,
      :twitter_handle, :wikipedia_url, :facebook_url, :linkedin_url)
  end
end
