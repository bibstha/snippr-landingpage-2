#updates the count if already exists
class UserAuthorSearch
  include Mongoid::Document
  include Mongoid::Token
  include Mongoid::Timestamps
  field :author_name, type: String
  field :is_available_in_snippr, type:Boolean, default: false
  field :users, type: Array
  field :search_count, type: Integer,default: 0
  
  def self.authorsearch(searched_author,seachresult,useremail,search_exists)

    
    author = UserAuthorSearch.find_by(:author_name => searched_author)
    
    if not author.blank?
      author.search_count += 1
      author.users ||= []
      author.users << useremail
      author.is_available_in_snippr= search_exists
      author.update
    else
      
      author = UserAuthorSearch.new
      author.author_name = searched_author
      author.users ||= []
      author.users << useremail
      author.search_count += 1
      author.is_available_in_snippr= search_exists
      author.save
    end

  end


end

# #insert new row for each search
# class UserAuthorSearch
#   include Mongoid::Document
#   include Mongoid::Token
#   include Mongoid::Timestamps
#   field :author_name, type: String
#   field :is_available_in_snippr, type:Boolean, default: false
#   field :users, type: String
  
  
#   def self.authorsearch(searched_author,seachresult,useremail,search_exists)

#       author = UserAuthorSearch.new
#       author.author_name = searched_author      
#       author.users = useremail      
#       author.is_available_in_snippr= search_exists
#       author.save   

#   end

# end

