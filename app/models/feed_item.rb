class FeedItem
  include Mongoid::Document
  include Mongoid::Token
  include Mongoid::Timestamps
  include Mongoid::Commentable
  
  field :title, type: String
  field :url, type: String
  field :author, type: String
  field :teaser, type: String
  field :summary, type: String
  field :content, type: String
  field :published, type: Time
  field :categories, type: Array
  field :entry_id, type: String
  field :is_crawled, type: Boolean
  field :crawled_date, type: Time
  field :feed_token, type: String

  field :like_count, type: Integer, default: 0
  field :view_count, type: Integer, default: 0
  field :comment_count, type: Integer, default: 0

  field :tags, type: Array
  field :flags, type: Hash, default: {}
  field :profile_tokens, type: Array, default: []
  token :length => 8

  def self.increase_like(token)
    feed_item = find(token)
    if feed_item
      feed_item.like_count += 1
      feed_item.save
    end
  end

  def self.decrease_like(token)
    feed_item = find(token)
    if feed_item
      feed_item.like_count -= 1 if feed_item.like_count > 0
      feed_item.save
    end
  end

  def all_profile_tokens
    profile_tokens = self.profile_tokens
    profile = Profile.find_by(feed_token: self.feed_token)
    profile_tokens << profile.token if profile and not profile_tokens.include? profile.token
    profile_tokens
  end
end
