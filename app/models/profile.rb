class Profile
  include Mongoid::Document
  include Mongoid::Token
  include Mongoid::Timestamps
  field :name, type: String
  field :short_description, type: String
  field :description, type: String
  field :photo_url, type: String
  field :twitter_handle, type: String
  field :wikipedia_url, type: String
  field :facebook_url, type: String
  field :linkedin_url, type: String
  field :web_urls, type: Array
  field :feed_tokens, type: Array, default: []
  field :score,type: Integer
  # field :feed_url, type: String
  token :length => 8

  field :temp, type: Hash
  field :flags, type: Hash
  field :tags, type: Array, default: []

  # @update_attrs = [
  #   :name, :short_description, :description, :photo_url,
  #   :twitter_handle, :wikipedia_url, :facebook_url, :linkedin_url
  # ]
  def update_profile_ranking    

    if twitter_handle.blank?
      profile = Profile.find_by(twitter_handle: twitter_handle)
      twitter_profile = TwitterUser.find_by(:screen_name => twitter_handle)
      if twitter_profile
        twitter_followers_count = twitter_profile.followers_count
        twitter_following_count = twitter_profillandingpage_production  e.friends_count

        latest_published_date = Feed.where(:token => {"$in" => profile.feed_tokens}).max(:last_modified)
        days_since_last_published = (Date.today - latest_published_date.to_date).to_i

        number_of_articles = FeedItem.where(:feed_token => {"$in" => profile.feed_tokens}).count
        # below is the logic to compute the score; higher the value higher is the importance
        score = twitter_followers_count-twitter_following_count- days_since_last_published + number_of_articles
        return score
      end
    end
  end

  def self.author_search(search_condition,current_user_email)

    author_search_result = Profile.and(
      [
        {"$or" => [{:name => /.*#{search_condition}.*/i},{:short_description => /.*#{search_condition}.*/i}]},
        {:"flags.disabled".exists => false}
      ]
    ).order_by(:score => -1)

      # collect the name of the author searched if author is not found
    if author_search_result.blank?
      search_exists = false
    else
      search_exists = true
    end
    
    UserAuthorSearch.authorsearch(search_condition,author_search_result,current_user_email,search_exists)

    return author_search_result
  
  end

  def author_recommend(current_user_token)

    #paul krug man 96ps
    #bill gates 6Wiw
    #eric ries zXSL
    # steve blank umQu
    # randi zuckerberg sKRe
    # mark suster qE0U
    # paul graham XqGU
    # Caterina Fake 6jfG
    # Loic Yj5R
    # josh kopelman MDm6
    # evan williams fyNI
    # dave morin ea6t
    # biz stone 
    # craig neward WO8l
    # mark cuban 
    # EMILY BAZELON 4d7c
    authors_to_recommend = ['96ps','6Wiw','zXSL','sKRe','qE0U','XqGU','6jfG','Yj5R','MDm6','fyNI','ea6t','4pbr','6xVr','WO8l','4d7c']
    current_user = User.find_by(token: current_user_token)    
    recommended_profiles = Profile.where( "$and" =>[{:token => {"$nin" => current_user.following_profiles}},
                                                    {:token => {"$in" => authors_to_recommend}}]).limit(20)
  end

  def feed_items
    FeedItem.or({:feed_token.in => [self.feed_tokens]}, {:profile_tokens => self.token})
  end

  def photo_url_fixed
    if self.photo_url
      self.photo_url
    else
      "http://placehold.it/73x73&text=."
    end
  end
end

 

