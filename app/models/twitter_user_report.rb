class TwitterUserReport
  include Mongoid::Document
  field :user_id, type: Integer
  field :screen_name, type: String
  field :name, type: String
  field :friends_count, type: Integer
  field :followers_count, type: Integer
  field :profile_image_url, type: String
  field :feed_links, type: Array
  field :has_feed_links, type: Boolean
  field :score_is_person, type: Float
end
