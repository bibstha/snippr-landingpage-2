class ModelViewLog
  include Mongoid::Document
  field :type, type: String
  field :type_token, type: String
  field :user_token, type: String
end
