class FeedParser::DynamicAuthorParser
  # feed_item - instance of FeedItem
  # feed - instane of Feed
  def process(feed_item, feed)
    puts "DynamicAuthor processing for #{feed_item.token}"
    author = feed_item.author
    profile = Profile.find_by(name: /#{author}/i)
    unless profile
      profile = Profile.new
      profile.name = author
      profile.save
    end
    feed_item.profile_tokens ||= []
    feed_item.profile_tokens << profile.token unless feed_item.profile_tokens.include?(profile.token)
    feed_item.save
  end
end
