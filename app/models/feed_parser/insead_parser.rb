require 'nokogiri'
require 'open-uri'

class FeedParser::InseadParser
  # feed_item - instance of FeedItem
  # feed - instane of Feed
  def process(feed_item, feed)
    puts "InseadParser::process FeedItem: #{feed_item.token}"
    url = feed_item.url
    doc = Nokogiri::HTML(open(url))
    authors = [doc.css('.article .author')[0].content]
    # authors = doc.css('.article .author')[0].content.split(",").map {|x| x.strip}
    profile_tokens = authors.map do |author|
      profile = Profile.find_by(name: /#{author}/i)
      unless profile
        profile = Profile.new
        profile.name = author
      end
      profile.tags ||= []
      profile.tags << 'insead' unless profile.tags.include?"insead"
      profile.save
      profile.token
    end
    feed_item.profile_tokens = profile_tokens
    feed_item.save
  end
end