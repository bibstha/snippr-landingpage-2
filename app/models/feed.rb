class Feed
  include Mongoid::Document
  include Mongoid::Token
  include Mongoid::Timestamps

  field :title, type: String
  field :url, type: String
  field :feed_url, type: String
  field :etag, type: String
  field :last_modified, type: Time
  field :create_date, type: Time # Time of creation
  field :update_date, type: Time # Time of update
  field :feed_class, type: String # Rss / Atom
  field :feed_item_class, type: String # RssItem / AtomItem

  field :feed_item_tags, type: Array
  field :flags, type: Hash #eg flags.disabled
  field :feed_item_parsers, type: Array #An ordered list of specific Parsers
  field :tags, type: Array
  token

  def feedzirra_object
    return @feedzirra_object
  end

  def self.create_by_feed_url(feed_url)
    self.find_by(feed_url: feed_url) or self.create(feed_url: feed_url)
  end

  def pull
    @feedzirra_object = nil
    if self.feed_class.blank? || (self.etag.blank? && self.last_modified.blank?)
      @feedzirra_object = Feedzirra::Feed.fetch_and_parse(feed_url)
      if @feedzirra_object == 0
        return nil
      end

      save_feed @feedzirra_object

      logger.debug("FeedCrawl - FetchAndParse: Found #{@feedzirra_object.entries.count} new items")
      @feedzirra_object.entries.each do |feed_item|
        feed_item_obj = save_feed_items feed_item
        if feed_item.content.blank?
          Resque.enqueue(FeedItemIndetailExtractor, feed_item_obj.token)
        end
        process_parsers feed_item_obj
      end
    else
      @feedzirra_object = self.feed_class.constantize.new
      @feedzirra_object.feed_url = self.feed_url
      @feedzirra_object.etag = self.etag
      @feedzirra_object.last_modified = self.last_modified

      last_feed_item = self.feed_item_class.constantize.new
      last_feed_item.url = FeedItem.where(feed_token: self.token).sort(published: -1).first.url

      @feedzirra_object.entries = [last_feed_item]
      updated_feeds = Feedzirra::Feed.update(@feedzirra_object)
      @feedzirra_object = updated_feeds
      logger.debug("FeedCrawl - Update: Found #{@feedzirra_object.new_entries.count} new items")
      @feedzirra_object.updated? and @feedzirra_object.new_entries.each do |feed_item|
        feed_item_obj = save_feed_items feed_item
        if feed_item.content.blank?
          Resque.enqueue(FeedItemIndetailExtractor, feed_item_obj.token)
        end
        process_parsers feed_item_obj
      end
    end
    true
  end

  def save_feed(feed)
    self.title = feed.title
    self.url = feed.url
    self.feed_url = feed.feed_url
    self.etag = feed.etag
    self.last_modified = feed.last_modified
    self.feed_class = feed.class.to_s
    if feed.entries.count > 0
      self.feed_item_class = feed.entries.first.class.to_s
    end
    save
  end

  def save_feed_items(feed_item)
    # Use url to check if something has been added
    feedItem = FeedItem.find_by({url: feed_item.url})
    if feedItem
      feedItem
    else
      feedItem = FeedItem.create({
        title: feed_item.title,
        url: feed_item.url,
        author: feed_item.author,
        summary: feed_item.summary,
        content: feed_item.content,
        published: feed_item.published,
        entry_id: feed_item.entry_id,
        # categories: feed_item.categories,
        feed_token: self.token,
        tags: self.feed_item_tags
      })
    end
  end

  def process_parsers(feed_item)
    puts "Flags: #{flags}"
    if flags and not flags["dynamic_author"].blank?
      p = FeedParser::DynamicAuthorParser.new
      p.process(feed_item, self)
    end

    if flags and not flags["insead"].blank?
      p = FeedParser::InseadParser.new
      p.process(feed_item, self)
    end
  end
end
