class Subscription
  include Mongoid::Document
  include Mongoid::Timestamps
  field :email, type: String
  field :following_authors, type: Array
  field :suggested_authors, type: Array
  field :created_date, type: DateTime
end
