class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Token
  token
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  ## Database authenticatable
  field :email,              :type => String, :default => ""
  field :encrypted_password, :type => String, :default => ""

  ## Recoverable
  field :reset_password_token,   :type => String
  field :reset_password_sent_at, :type => Time

  ## Rememberable
  field :remember_created_at, :type => Time

  ## Trackable
  field :sign_in_count,      :type => Integer, :default => 0
  field :current_sign_in_at, :type => Time
  field :last_sign_in_at,    :type => Time
  field :current_sign_in_ip, :type => String
  field :last_sign_in_ip,    :type => String

  ## Confirmable
  # field :confirmation_token,   :type => String
  # field :confirmed_at,         :type => Time
  # field :confirmation_sent_at, :type => Time
  # field :unconfirmed_email,    :type => String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, :type => Integer, :default => 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    :type => String # Only if unlock strategy is :email or :both
  # field :locked_at,       :type => Time

  ## Token authenticatable
  # field :authentication_token, :type => String

  after_create :assign_default_profiles_to_users

  field :display_name, type: String
  field :full_name, type: String

  field :readlist_tokens, type: Array
  field :following_profiles, type: Array, default: []
  field :favorite_articles, type: Array
  field :first_time, type: Boolean, :default => true
  field :photo_url, type: String

  validates :display_name, presence: true, length: {maximum: 20}

  def following_profile?(profile_token)
    if following_profiles.blank?
      false
    else
      following_profiles.include?(profile_token)
    end
  end

  def my_readlists
    if self.readlist_tokens.blank?
      return []
    else
      Readlist.where(token: {"$in" => self.readlist_tokens})
    end
  end

  def my_following_profiles
    if self.following_profiles.blank?
      return []
    else
      Profile.where(token: {"$in" => self.following_profiles})
    end
  end

  def save_favorite_articles(feed_token) 
    
    self.favorite_articles ||= []
    self.favorite_articles.insert({feed_token: feed_token, favorited_on: DateTime.now})
    self.save

  end

  def photo_url_fixed
    if self.photo_url
      self.photo_url
    else
      "http://placehold.it/73x73&text=."
    end
  end
  
  private
  def assign_default_profiles_to_users
    # richard branson V7mw
    # amy good man 9no8
    # steven pinker 3NC8
    # marissa mayer 3u2E
    # thomas friedman YHvE
    # richard dwakins wHYr
    # self.following_profiles = ['V7mw','9no8','3NC8','3u2E','YHvE','wHYr']
    # self.save
  end
end
