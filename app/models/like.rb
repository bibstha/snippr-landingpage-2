class Like
  include Mongoid::Document
  field :type, type: String
  field :type_token, type: String
  field :user_token, type: String

  def self.toggle(type, type_token, user_token)
    like = find_by(type: type, type_token: type_token, user_token: user_token)
    if like
      like.delete
      # TODO convert this to a generic solution
      FeedItem.decrease_like(type_token) if type == "feed_item"
      false
    else
      create(type: type, type_token: type_token, user_token: user_token)
      FeedItem.increase_like(type_token) if type == "feed_item"
      true
    end
  end
end
