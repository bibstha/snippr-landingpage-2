class Comment
  include Mongoid::Document
  include Mongoid_Commentable::Comment
  field :text, :type => String
  field :author, :type => String
  field :deleted, :type => Boolean, default: false

  def profile
    profile = User.find(self.author)
  end
end
