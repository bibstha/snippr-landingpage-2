class TwitterUser
  include Mongoid::Document
  field :user_id, type: Integer
  field :screen_name, type: String
  field :name, type: String
  field :location, type: String
  field :description, type: String
  field :expanded_url, type: String
  field :friends_ids, type: Array
  field :followers_count, type: Integer
  field :friends_count, type: Integer

  # The actual json data obtained from Twitter
  field :users_lookup_json_data, type: String
  field :friends_ids_json_data, type: String
  field :friends_ids_done, type: Boolean # relates with /friends/ids.json
  field :users_lookup_done, type: Boolean # relates with /users/lookup.json

  field :feed_links_parse_done, type: Boolean
  field :feed_links, type: Array

  field :import_to_profile_done, type: Boolean

  @@id_vs_screen_name_selector = lambda { |x| x ? :screen_name : :user_id }

  # Crawls /usrs/lookup.json for users' basic information
  def self.pull_users_lookup(ids, use_screen_name=false)
    unless ids.kind_of?(Array)
      ids = ids.split(",")
    end

    # @todo take care of rejected later
    filtered_ids = ids.reject { |id| self.users_lookup_done?(id, use_screen_name) }
    if filtered_ids.blank?
      throw :no_users_to_crawl
    end
    access_token = self.twitter_token_for_snippr

    url = "https://api.twitter.com/1.1/users/lookup.json?%s=%s" % 
      [(use_screen_name ? :screen_name : :user_id), filtered_ids.join(",")]
    
    response = access_token.request(:get, url)

    # @todo manage error coonditions later
    twitter_users = nil
    if response.code == "200"
      twitter_users = self.from_json response.body
      twitter_users.each do |t_user|
        t_user.save
      end
    else
      p response.body
      throw :invalid_response_from_api_call
    end

    twitter_users
  end

  def self.pull_friends_ids(id, use_screen_name=false)
    twitter_user = self.user_exists?(id, use_screen_name)
    if not twitter_user
      throw :twitter_user_not_found
    end

    if self.friends_ids_done?(id, use_screen_name)
      return nil
    end

    access_token = self.twitter_token_for_snippr
    friends_ids_url = "https://api.twitter.com/1.1/friends/ids.json?%s=%s" % [@@id_vs_screen_name_selector.call(use_screen_name), id]

    response = access_token.request(:get, friends_ids_url)
    if response.code == "200"
      twitter_user.friends_ids_json_data = response.body
      twitter_user.friends_ids_done = true
      twitter_user.friends_ids = JSON.parse(response.body)["ids"]
      twitter_user.save
    else
      puts "Error calling %s\nError: %s" % [friends_ids_url, response.body]
      throw :invalid_response_from_api_call
    end
    twitter_user
  end

  def self.user_exists?(id, use_screen_name=false)
    if not use_screen_name
      self.find_by user_id: id
    else
      self.find_by screen_name: /^#{id}$/i
    end
  end

  def self.users_lookup_done?(id, use_screen_name = false)
    twitter_user = self.user_exists?(id, use_screen_name) 
    if twitter_user and twitter_user.users_lookup_done
      return true
    else
      return false
    end
  end

  def self.friends_ids_done?(id, use_screen_name=false)
    twitter_user = self.user_exists?(id, use_screen_name)
    twitter_user and twitter_user.friends_ids_done
  end

  def self.twitter_token_for_snippr
    oauth_token = "17726589-O6qMqoqOgQqyII8GBbzsFG0vRO7sGyPaZxKhcYCOE"
    oauth_token_secret = "O5zSuFpRRoupufVWx9crg8e0MjQ8a5E4rigblifQWj0"
    api_key = "bwyht6dEa95xk0G68RhWA"
    api_secret = "xi2HwBa9AY2jTSMEPLkmrm7PZpFZABkqKwJEjh8UUs"
    consumer = OAuth::Consumer.new(api_key, api_secret, 
      { :site => "http://api.twitter.com",
        :scheme => :header
      })
    # now create the access token object from passed values
    token_hash = { :oauth_token => oauth_token,
                   :oauth_token_secret => oauth_token_secret
                 }
    access_token = OAuth::AccessToken.from_hash(consumer, token_hash)
    return access_token
  end

  def self.from_json(json_string)
    result = []
    json = JSON.parse(json_string)
    json.each do |json_user|
      unless json_user["id"].blank?
        twitter_user = TwitterUser.new
        twitter_user.user_id = json_user["id"]
        twitter_user.screen_name = json_user["screen_name"]
        twitter_user.name = json_user["name"]
        twitter_user.location = json_user["location"]
        twitter_user.description = json_user["description"]
        if not json_user["entities"].blank? and not json_user["entities"]["url"].blank?
          twitter_user.expanded_url = json_user["entities"]["url"]["urls"][0]["expanded_url"]
        end
        twitter_user.followers_count = json_user["followers_count"]
        twitter_user.friends_count = json_user["friends_count"]
        twitter_user.users_lookup_json_data = JSON.generate(json_user)
        twitter_user.users_lookup_done = true
        result << twitter_user
      end
    end

    result = nil if result.blank?
    result
  end

  def filtered_feeds
    return [] if feed_links.blank?
    scan_urls = []
    without_duplicate_feed_links = feed_links.select do |feed|
      if scan_urls.include? feed["href"]
        false
      else
        scan_urls << feed["href"]
        true
      end
    end

    without_comments_feed_link = without_duplicate_feed_links.select do |feed_link|
      if feed_link["title"] && feed_link["title"].downcase.include?("comment")
        false
      elsif feed_link["href"].blank? || feed_link["href"].downcase.include?("comment")
        false
      else
        true
      end
    end

    # If contains both RSS and ATOM, filter out the ATOMS
    without_comments_feed_link.is_a?(Array) and without_comments_feed_link.count > 1 and 
      non_atom_links = without_comments_feed_link.select do |feed_link|
      if (not feed_link["title"].blank?) && feed_link["title"].downcase.include?("atom")
        false
      elsif feed_link["href"].downcase.include?("atom")
        false
      else
        true
      end
    end

    if non_atom_links.is_a?(Array) && non_atom_links.count > 0
      non_atom_links
    else
      without_comments_feed_link
    end
  end
end
