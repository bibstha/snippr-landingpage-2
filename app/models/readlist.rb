class Readlist
  include Mongoid::Document
  include Mongoid::Token
  include Mongoid::Timestamps
  field :title, type: String
  field :profile_tokens, type: Array
  token
  field :is_public, type: Boolean,default: false
end
