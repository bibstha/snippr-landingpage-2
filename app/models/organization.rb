class Organization
  include Mongoid::Document

  @@org_name_tag_map = {
    'standford-news-insights' => 'Stanford',
    'insead-blog' => 'INSEAD',
    'havard-business-school' => 'Havard',
    'dartmouth-newsroom' => 'Dartmouth',
    'columbia-global-insights' => 'Chicago',
    'kellog-blogs' => 'Kellogg',
    'imd-tomorrows-challenges' => 'IMD',
  }

  def self.name_for tag
    return @@org_name_tag_map[tag]
  end

  def self.org_name_tag_map
    return @@org_name_tag_map
  end
end
