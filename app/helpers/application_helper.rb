module ApplicationHelper
  @@m = TactfulTokenizer::Model.new
  @@para_cutoff_len = 400
  @@para_sentence_len_max = 2

  def feed_summary(string)    
    text_version = strip_tags(string)
    text_version ||= ""
    sentences = @@m.tokenize_text text_version[0..@@para_cutoff_len]
    return sentences[0..(@@para_sentence_len_max-1)].join(" ").html_safe
  end
end