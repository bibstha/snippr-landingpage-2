// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.


function FeedAuthorListCtrl($scope, $http, AuthorList) {
  $scope.authorlist = new AuthorList();
  // each object in readlist contains readlist: { token: "", title: "", profile_tokens: [] }
  $scope.my_readlists = {};

  // store following / unfollowing state of any given profile for a user
  // token: { name: "", token: "", not_following: true/false }
  $scope.profiles = {};

  $scope.getProfile = function(token) {
    if ( !(token in $scope.profiles) ) {
      $scope.profiles[token] = {name: "", token: token, not_following: true};
    }
    
    return $scope.profiles[token];
  }
  
  $scope.init = function() {
    $scope.updateReadlists();
    $scope.updateProfiles();
  }

  // Get all readlist for current user
  $scope.updateReadlists = function() {
    $http.get(["/users/", USER_TOKEN, "/readlists"].join(""))
    .success(function(data) {
      $scope.my_readlists = data.readlists;
    });
  }

  // Get all following profiles for current user
  $scope.updateProfiles = function() {
    $http.get(["/users/", USER_TOKEN, "/following_profiles"].join(""))
    .success(function(data) {
      for (var attr in data.following_profiles) {
        $scope.profiles[attr] = data.following_profiles[attr]
      }
    });
  }
}

// Timeline constructor function to encapsulate HTTP and pagination logic
app.factory('AuthorList', function($http) {
  var AuthorList = function() {
    this.authors = [];
    this.busy = false;
    this.after = 1;
  };

  AuthorList.prototype.nextPage = function() {
    if (this.busy) return;
    this.busy = true;

    var url = window.location.toString()
    if (url.indexOf("?") == -1) {
      url = url + "?page=" + this.after;
    }
    else {
      url = url + "&page=" + this.after;
    }

    $http.get(url).success(function(data) {
      var authors = data;
      for (var i = 0; i < authors.length; i++) {
        this.authors.push(authors[i]);
      }
      this.after = this.after+1;
      this.busy = false;
    }.bind(this));
  };
  return AuthorList;
});

FeedAuthorShowPostsCtrl = FeedAuthorListCtrl;