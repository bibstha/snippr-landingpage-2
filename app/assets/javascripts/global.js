app = angular.module('snipprapp', ['infinite-scroll']);
app.directive('followbutton', function($http){
  return {
    restrict: 'EC',
    replace: true,
    // transclude: true,
    scope: { 
      target: '=',
    },
    template: '<a class="{{ getClass() }}">{{ getLabel() }}</a>',
    link: function(scope, element, attrs, controller) {
      scope.getLabel = function() {
        return (scope.target.not_following) ? "Follow":"Following";
      }

      scope.getClass = function() {
        return (scope.target.not_following)?"btn-primary":"btn-success"
      }

      element.bind('click', toggle);
      function toggle() {
        scope.$apply(function() {
          $http.post(["/authors/", scope.target.token, "/follow"].join(""),
            { authenticity_token: AUTH_TOKEN })
          .success(function(data) {
            scope.target.not_following = !data.value;
          });
        });
      }
    }
  }
});

app.directive('likebutton', function($http){
  var states = {
    "default": {label: "Like", "class": "lb-default-class"},
    "opposite": {label: "Unlike", "class": "lb-opposite-class"}
  };

  var clickHandler = function($scope) {
    $http.post("/like", {
      authenticity_token: AUTH_TOKEN,
      object_type: $scope.objectType,
      object_token: $scope.objectToken
    })
    .success(function(data) {
      $scope.defaultState = !data.value;
    });
  }

  return {
    restrict: 'EC',
    replace: true,
    // transclude: true,
    scope: { 
      defaultState: '=',
      objectType: '@',
      objectToken: '@'
    },
    template: '<a class="{{ getClass() }}"><i class="fa fa-thumbs-o-up"></i> {{ getLabel() }}</a>',
    link: function($scope, $element, attrs, $controller) {
      $scope.getLabel = function() {
        return (($scope.defaultState) ? states["default"] : states["opposite"]).label
      }
      $scope.getClass = function() {
        return (($scope.defaultState) ? states["default"] : states["opposite"]).class
      }

      $element.bind('click', toggle);
      function toggle() {
        $scope.$apply(clickHandler, $scope);
      }
    }
  }
});

app.directive('readlistprivacybutton', function($http){
  return {
    restrict: 'EC',
    replace: true,
    // transclude: true,
    scope: { 
      target: '=',
    },
    template: '<a class="{{ getClass() }}">{{ getLabel() }}</a>',
    link: function(scope, element, attrs, controller) {
      scope.getLabel = function() {
        return (scope.target.is_public) ? "Public":"Private";
      }

      scope.getClass = function() {
        return (scope.target.is_public)?"btn-primary":"btn-success"
      }

      element.bind('click', toggle);
      function toggle() {
        //scope.target.is_public = !scope.target.is_public;
        scope.$apply(function() {
             $http.post(["/readlist/", scope.target.token, "/privacy"].join(""),
            { authenticity_token: AUTH_TOKEN })
          .success(function(data) {
            scope.target.is_public = !scope.target.is_public;
          });
        })
        // $http.post(["/authors/", scope.target.token, "/follow"].join(""),
        //   { authenticity_token: AUTH_TOKEN })
        // .success(function(data) {
        //   scope.target.is_public = !data.value;
        // });
        
      }
    }
  }
});

app.directive('readlistbutton', function($http, $compile){

  var modalTemplate = '<div id="modal-{{target.token}}" class="modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' +
    '<form class="modal-dialog" ng-submit="new_readlist_modal.onSaveModal()">' +
    '<div class="modal-content">' + 
      '<div class="modal-header">' +
        '<a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>' +
        '<h4 class="modal-title">Name of the Read List</h4>' +
      '</div><div class="modal-body">' +
        '<input class="form-control" name="name" ng-model="new_readlist_modal.name" placeholder="Read List Name"></input>' +
      '</div><div class="modal-footer">' +
        '<input class="btn btn-default" type="button" data-dismiss="modal" value="Cancel"></input>' + 
        '<input class="btn btn-primary" type="submit" value="Save"></input>' +
      '</div>' +
    '</div></form></div>';
  
  return {
    restrict: 'EC',
    replace: true,
    transclude: true,
    scope: { 
      target: '=',
      readlistsModel: '=',
      after: '&'
    },
    template: 
      '<div class="btn-group" ng-transclude>' +
          // '<span ng-transclude></span>' +
          // '<a class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">Add to Readlist '+
          // '<span class="caret"></span></a>' +
          '<ul class="dropdown-menu">' +
            '<li ng-repeat="readlist in readlistsModel">' + 
              '<a ng-click="onReadlistClick(readlist.title)">' +
              '<i class="{{ getCheckMark(readlist) }}"></i> {{readlist.title}}</a>' +
            '</li>' +
            '<li class="divider"></li>' +
            '<li><a ng-click="new_readlist_modal.onShowModal()">New Readlist</a></li>' +
          '</ul>' +
        '</div>',
    link: function(scope, element, attrs, controller) {

      scope.new_readlist_modal = {
        name: "",
        onShowModal: function() {
          if (undefined == scope.modalTemplate) {
            scope.modalTemplate = $compile(modalTemplate)(scope);
            $('body').append(scope.modalTemplate);
          }
          
          setTimeout(function() {
            $('#modal-' + scope.target.token).modal('show');
          }, 200);
          
        },
        onSaveModal: function() {
          scope.onReadlistClick(scope.new_readlist_modal.name);
          scope.new_readlist_modal.name = "";
          $('#modal-' + scope.target.token).modal('hide');
        }
      }

      scope.onReadlistClick = function(readlist_title) {
        $http.post("/users/readlists", {
          title: readlist_title, 
          authenticity_token: AUTH_TOKEN,
          profile_token: scope.target.token,
        })
        .success(function(data) {
          scope.after();
        });
      }

      scope.getCheckMark = function(readlist) {
        if (-1 < $.inArray(scope.target.token, readlist.profile_tokens)) {
          return 'fa fa-check';
        }
        else {
          return '';
        }
      }
    }
  }
});

function HomePageCtrl($scope) {
  $scope.following = [];
  $scope.authors = [
    { 'id': 'branson',
      'name': "Richard Branson",
      'description': "Founder of Virgin Groups"
    },
    {
      'id': 'chomsky',
      'name': "Noam Chomsky",
      'description': "Liguist, Philosopher, Cognitive Scientist, Logician"
    },
    {
      'id': 'friedman',
      'name': "Thomas L Friedman",
      'description': "Author, Columnist at The New York Times"
    },
    {
      'id': 'gates',
      'name': "Bill Gates",
      'description': "Entrepreneur, Philanthropist, Cofounder of Microsoft"
    },
    {
      'id': 'rowling',
      'name': "J.K. Rowling",
      'description': "Novelist, Harry Potter"
    },
    {
      'id': 'sandberg',
      'name': "Sheryl Sandberg",
      'description': "COO at Facebook, Author of Lean In"
    }
  ];
  $scope.suggested_authors = [];
  $scope.email = "";

  $scope.follow = function(event, author_id) {
    var id = author_id;
    event.preventDefault();
    var index = $.inArray(id, $scope.following);
    if (index < 0) {
      $scope.following.push(id);
    } else {
      $scope.following.splice(index, 1);
    }
  }

  $scope.isFollowing = function(author_id) {
    var id = author_id;
    if ($.inArray(id, $scope.following) < 0) {
      return false;
    }
    else {
      return true;
    }
  }

  $scope.submitForm = function() {
    $('#new_subscription').validate({
      submitHandler: function() {
        $.post("/subscribe",
          {
            'authenticity_token': $('input[name=authenticity_token]').val(),
            'following': $scope.following,
            'suggested_authors': $scope.suggested_authors,
            'email': $scope.email
          }
        ).success(function(data) {
          $('#thankyouModal').modal('show');
        });
      }
    });
  }
}



$(document).ready( function() {
  $(".scroll").click(function(event){
    event.preventDefault();
    $("html, body").animate({ scrollTop: $(document).height() }, 1000);
  });
});


(function(e,b){if(!b.__SV){var a,f,i,g;window.mixpanel=b;a=e.createElement("script");a.type="text/javascript";a.async=!0;a.src=("https:"===e.location.protocol?"https:":"http:")+'//cdn.mxpnl.com/libs/mixpanel-2.2.min.js';f=e.getElementsByTagName("script")[0];f.parentNode.insertBefore(a,f);b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==
typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");for(g=0;g<i.length;g++)f(c,i[g]);
b._i.push([a,e,d])};b.__SV=1.2}})(document,window.mixpanel||[]);
mixpanel.init("fb4016e9304c18de8d9dd00f923454d2");

$(document).ready(function(){
  mixpanel.track_forms("#new_user", "Signed in");
  mixpanel.track("New Value");  
  mixpanel.track_links(".timeline .title-click a", "read article");
});
