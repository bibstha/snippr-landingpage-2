function FeedPostViewCtrl($scope, $http) {
  $scope.defaultState = true;
  $scope.feed_item_token = '';

  $scope.my_readlists = {};

  // store following / unfollowing state of any given profile for a user
  // token: { name: "", token: "", not_following: true/false }
  $scope.profiles = {};
  
  $scope.init = function(feed_item_token) {
    $scope.updateReadlists();
    $scope.updateProfiles();
    
    $scope.feed_item_token = feed_item_token;
    $http.get("/likes/feed_item/" + $scope.feed_item_token).success(function(data) {
      $scope.defaultState = !data.value;
    });
  }

  // Get all readlist for current user
  $scope.updateReadlists = function() {
    $http.get(["/users/", USER_TOKEN, "/readlists"].join(""))
    .success(function(data) {
      $scope.my_readlists = data.readlists;
    });
  }

  // Get all following profiles for current user
  $scope.updateProfiles = function() {
    $http.get(["/users/", USER_TOKEN, "/following_profiles"].join(""))
    .success(function(data) {
      for (var attr in data.following_profiles) {
        $scope.profiles[attr] = data.following_profiles[attr]
      }
    });
  }

  $scope.getProfile = function(token) {
    if ( !(token in $scope.profiles) ) {
      $scope.profiles[token] = {name: "", token: token, not_following: true};
    }
    
    return $scope.profiles[token];
  }
}