app.controller('FeedTimelineCtrl', function($scope, Timeline) {
  $scope.timeline = new Timeline();
});

// Timeline constructor function to encapsulate HTTP and pagination logic
app.factory('Timeline', function($http) {
  var Timeline = function() {
    this.items = [];
    this.busy = false;
    this.after = 1;
  };

  Timeline.prototype.nextPage = function() {
    if (this.busy) return;
    this.busy = true;

    var url = "/timeline?page=" + this.after;
    $http.get(url).success(function(data) {
      var items = data;
      for (var i = 0; i < items.length; i++) {
        this.items.push(items[i]);
      }
      this.after = this.after+1;
      this.busy = false;
    }.bind(this));
  };
  return Timeline;
});