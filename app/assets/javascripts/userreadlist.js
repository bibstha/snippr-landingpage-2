// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.


function ReadlistIspublicCtrl($scope, $http) {
  // btnstate = {'tokenid': {is_public: true/false}}
  $scope.btnstate = {};

  // each object in readlist contains readlist: { token: "", title: "", profile_tokens: [] }
  $scope.readlist = {};

  // store following / unfollowing state of any given profile for a user
  // token: { name: "", token: "", not_following: true/false }
  

    // Get all readlist for current user
  $scope.getBtnState = function(token) {
    if (!(token in $scope.btnstate))
      $scope.btnstate[token] = {token: token, is_public: false};
    return $scope.btnstate[token];
  }
  $scope.init= function(){
    $scope.updateReadlists();
  }

  $scope.updateReadlists = function(){
    $http.get(["/users/", USER_TOKEN, "/readlists"].join(""))
    .success(function(data) {
      for(var attr in data.readlists){
        $scope.btnstate[attr] = data.readlists[attr]

      }
      
    });

  }

    
}  



