// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

angular.module('snippradmin', []).
  config(['$routeProvider', function($routeProvider) {
  $routeProvider.
      when('/', {templateUrl: 'profile-index.html', controller: ProfileIndexCtrl}).
      when('/create-new', {templateUrl: 'profile-show.html', controller: ProfileShowCtrl}).
      when('/:token', {templateUrl: 'profile-show.html', controller: ProfileShowCtrl}).
      otherwise({redirectTo: '/'});
}]);

function ProfileIndexCtrl($scope, $http) {
  $scope.authors = [];

  $http.get('/admin/authors.json').success(function(data) {
    $scope.authors = data;
  });

  $scope.name = "";
  $scope.short_description = ""; 
  $scope.description = ""; 
  $scope.photo_url = "";  
  $scope.twitter_handle = "";  
  $scope.wikipedia_url = "";
  $scope.facebook_url = "";
  $scope.linkedin_url = "";
  $scope.feed_url = "";  

  $scope.addAuthor = function() {
    $http.post('/admin/authors.json', {
      name:                 $scope.name,
      short_description:    $scope.short_description,
      description:          $scope.description,
      photo_url:            $scope.photo_url,
      twitter_handle:       $scope.twitter_handle,
      wikipedia_link:       $scope.wikipedia_url,
      facebook_link:        $scope.facebook_url,
      linkedin_link:        $scope.linkedin_url,      
      feed_url:             $scope.feed_url,
      authenticity_token:   AUTH_TOKEN
    }).success(function(data) {
      $scope.authors.push({
        name:               data.name,
        short_description:  data.short_description,
        description:        data.description, 
        photo_url:          data.photo_url,       
        twitter_handle:     data.twitter_handle,
        wikipedia_url:      data.wikipedia_url,
        facebook_url:       data.facebook_url,
        linkedin_url:       data.linkedin_url,        
        click_url:          data.click_url,
        token:              data.token
      });

      $scope.name = "";
      $scope.short_description = ""; 
      $scope.description = ""; 
      $scope.photo_url = "";  
      $scope.twitter_handle = "";  
      $scope.wikipedia_url = "";
      $scope.facebook_url = "";
      $scope.linkedin_url = "";
      $scope.feed_url = ""; 
    
    });
  }

  $scope.deleteAuthor = function(token) {
    var answer = confirm("Delete?");
    if (answer) {
      $http.delete('/admin/authors/' + token + '?authenticity_token=' + encodeURIComponent(AUTH_TOKEN));
      var oldAuthors = $scope.authors;
      $scope.authors = [];
      angular.forEach(oldAuthors, function(author) {
        if (author.token != token) { 
          $scope.authors.push(author); 
        }
      });
    }
  }
}

function ProfileShowCtrl($scope, $http, $routeParams, $location) {
  $scope.profile = {};
  $scope.feeds = [];
  $scope.new_feed_url = "";
  $scope.token = $routeParams["token"]; 

  $scope.loadAuthor = function(token) {
    $http.get('/admin/authors/' + encodeURIComponent($scope.token) + ".json")
    .success(function(data) {
      $scope.profile = data["profile"];
      $scope.feeds = data["feeds"];
    })
    .error(function(data) {
      $location.path('/');
    });
  };

  $scope.save = function() {
    var func;
    var url;
    if ($scope.token) {
      func = $http.put;
      url = "/admin/authors/" + encodeURIComponent($scope.token) + ".json";
    } else {
      func = $http.post;
      url = "/admin/authors/";
    }
    func(url, {
      profile: $scope.profile,
      authenticity_token: AUTH_TOKEN
    }).success(function(data) {
      if (!$scope.token) {
        $location.path('/' + data["profile"]["token"]);
      } else {
        
      }
    });
  }

  $scope.add_feed = function() {
    if ("" != $scope.new_feed_url.trim()) {
      $scope.feeds.push({
        feed_url: $scope.new_feed_url.trim(),
        is_new: true
      });
      $scope.new_feed_url = "";
    }
  }

  $scope.delete_feed = function(feed) {
    var r = confirm("Do you REALLY want to delete?");
    if (r!=true) {
      return;
    }
    if (feed.is_new) {
      var old_feeds = $scope.feeds;
      $scope.feeds = [];
      angular.forEach(old_feeds, function(old_feed) {
        if (old_feed != feed) {
          $scope.feeds.push(old_feed);
        }
      });
    }
    else {
      var url = ["/admin/authors/", $scope["token"], "/feed/", feed.token, "?authenticity_token=", encodeURIComponent(AUTH_TOKEN)].join("")
      $http.delete(url).success(function() {
        var old_feeds = $scope.feeds;
        $scope.feeds = [];
        angular.forEach(old_feeds, function(old_feed) {
          if (old_feed != feed) {
            $scope.feeds.push(old_feed);
          }
        });
      })
    }
  }

  $scope.save_feeds = function() {
    $http.post("/admin/authors/feeds/" + encodeURIComponent($scope.token) + ".json", {
      feeds: $scope.feeds,
      authenticity_token: AUTH_TOKEN,
    }).success(function(data) {
      $scope.loadAuthor($scope.token);
      angular.forEach($scope.feeds, function(feed) {
        feed.is_new = false;
      })
    });
  }

  $scope.reset_feeds = function() {
    $scope.loadAuthor($scope.token);
  }

  if ($scope.token) {
    $scope.loadAuthor($scope.token);
  }
  else {
    // Creating new author  
  }
}

