require 'extensions/crawl_logger'

class FeedCrawler
  @queue = :feedcrawler
  def self.perform(feed_token)
    CRAWL_LOGGER.info("Starting FeedCrawler for #{feed_token}")
    feed = Feed.find(feed_token)
    feed.pull
  end
end

if __FILE__ == $0
  # Resque.enqueue(TwitterUserFriendsCrawler, "biancawelds", true)
  # TwitterUserFriendsCrawler.perform('nisselson', true)
  # FeedCrawler.perform('YGvD') # TheBakery
  FeedCrawler.perform('LIdQ')
end