class TwitterUserFeedParser
  @queue = :twitter_user_feed_parser

  def self.perform(screen_name)
    twitter_user = TwitterUser.find_by(screen_name: /^#{screen_name}$/i)
    if not twitter_user
      throw :user_not_found_in_db
    end

    if twitter_user.feed_links_parse_done
      throw :feed_already_parsed
    end

    if twitter_user.expanded_url.blank?
      return nil
    end

    parser = FeedLinkParser.new(twitter_user.expanded_url)
    parser.parse
    twitter_user.feed_links = parser.feed_links
    twitter_user.feed_links_parse_done = true
    twitter_user.save

    unless twitter_user.feed_links.blank?
      Resque.enqueue(TwitterUserToProfileImporter, twitter_user.screen_name)
    end
  end
end

if __FILE__ == $0
  TwitterUserFeedParser.perform("biancawelds")
end