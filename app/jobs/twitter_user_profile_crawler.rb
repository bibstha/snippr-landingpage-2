require 'extensions/crawl_logger'

class TwitterUserProfileCrawler

  @queue = :twitter_user_profile_crawler
  
  # ids can be a single number, screen name or combined id or screennames with comman
  def self.perform(ids, use_screen_name=false)
    # Main information about the users
    twitter_users = TwitterUser.pull_users_lookup(ids, use_screen_name)
    twitter_users.each do |twitter_user|
      Resque.enqueue(TwitterUserFriendsCrawler, twitter_user.screen_name, true)
      Resque.enqueue(TwitterUserFeedParser, twitter_user.screen_name)
    end
  end
end

if __FILE__ == $0
  if TwitterUser.all.count < 1
    Resque.enqueue(TwitterUserProfileCrawler, "nisselson", true)
  end
end