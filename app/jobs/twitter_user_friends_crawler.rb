class TwitterUserFriendsCrawler

  @queue = :twitter_user_friends_crawler
  
  # id can be a twitter user_id or screen_name
  # if id is blank, the first uncrawled id is taken from the database
  def self.perform(id, use_screen_name=false)
    if id.blank?
      tu = TwitterUser.find_by(friends_ids_done: {"$ne" => true})
    else
      tu = TwitterUser.user_exists?(id, use_screen_name)
    end
    
    if tu
      tu = TwitterUser.pull_friends_ids(tu.user_id)
      # filter out all previously crawled ids
      filtered_friends_ids = tu.friends_ids.reject { |id| TwitterUser.users_lookup_done?(id) }
      filtered_friends_ids.each_slice(100) do |friends| # for 100 users, do a single query crawl
        Resque.enqueue(TwitterUserProfileCrawler, friends.join(","))
      end
    else
      throw :user_not_found
    end
  end
end

if __FILE__ == $0
  # Resque.enqueue(TwitterUserFriendsCrawler, "biancawelds", true)
  # TwitterUserFriendsCrawler.perform('nisselson', true)
  TwitterUserFriendsCrawler.perform('SWITTERmrossi', true)
end