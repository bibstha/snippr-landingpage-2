require 'AlchemyAPI.rb'

class FeedItemIndetailExtractor
  @queue = :feed
  def self.perform(feed_item_token)
    feed_item = FeedItem.find(feed_item_token)
    if feed_item and (not feed_item.is_crawled)
      response = self.extract(feed_item.url)
      if not response.blank?
        feed_item.content = response
        feed_item.is_crawled = true
        feed_item.crawled_date = Time.now
        feed_item.flags[:crawled] = true
        feed_item.save!
      else
        throw "Cound not parse #{feed_item.url}"
      end
    end
  end

  def self.extract(url)
    self.extractRubyReadability(url) or self.extractLocalParser(url) or 
      self.extractReadabilityParser(url) or self.extractAlchemy(url) or nil
  end

  def self.extractAlchemy(url)
    api = AlchemyAPI.new
    api.setAPIKey "307e21c29f9999c117efd7d85997812f1ea311d2" 

    response = api.URLGetText(url, AlchemyAPI::OutputMode::JSON)
    response = JSON.parse(response)
    if "ok" == response["status"].downcase
      if not response["text"].blank?
        response["text"].gsub("\n", "<br/>")
      else
        nil
      end
    else
      nil
    end
  end

  def self.extractReadabilityParser(feed_url)
    api_url = "https://www.readability.com/api/content/v1/parser"
    api_token = "91ccd76a98d903d599970cd5ffe51f5973cd0be3"
    
    response = HTTParty.get(api_url, :query => {token: api_token, url: feed_url})
    if response["content"].blank?
      nil
    else
      response["content"]
    end
  end

  def self.extractLocalParser(feed_url)
    api_url = "http://localhost:5000/parser?url=#{feed_url}"
    response = HTTParty.get(api_url)
    if response.body.blank?
      nil
    else
      JSON.parse(response.body)["cleaned_text"]
    end
  end

  def self.extractRubyReadability(feed_url)
    require 'rubygems'
    require 'readability'
    require 'open-uri'

    source = open(feed_url).read
    rbody = Readability::Document.new(source, :tags => %w[div p img a], :attributes => %w[src href], :remove_empty_nodes => false)
    rbody.content
  end
end

if __FILE__ == $0
  # Resque.enqueue(FeedItemIndetailExtractor, 'VwdX')
  # FeedItemIndetailExtractor.perform('VwdX')
  # response = FeedItemIndetailExtractor.extractReadabilityParser('http://www.richarddawkins.net/foundation_articles/2013/9/21/why-women-will-save-the-atheist-skeptic-movement')
  # response = FeedItemIndetailExtractor.extractReadabilityParser('http://blog.biancawelds.com/2013/10/the-whisper-of-a-ghost/')
  # url1 = 'http://www.richarddawkins.net/foundation_articles/2013/9/21/why-women-will-save-the-atheist-skeptic-movement'
  # url2 = 'http://blog.biancawelds.com/2013/10/the-whisper-of-a-ghost/'
  # # response = FeedItemIndetailExtractor.extract(url1)
  # response = FeedItemIndetailExtractor.extract(url1)
  # p response
  content = FeedItemIndetailExtractor.extractReadabilityParser("http://www.nytimes.com/2013/10/27/opinion/sunday/bruni-italy-breaks-your-heart.html")
  puts content
end