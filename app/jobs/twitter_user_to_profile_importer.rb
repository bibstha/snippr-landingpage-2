class TwitterUserToProfileImporter
  @queue = :twitter_user_to_profile_importer

  def self.perform(screen_name)
    importer = TwitterUserToProfileImporter.new(screen_name)
    importer.import
  end

  def initialize(screen_name)
    @screen_name = screen_name
  end

  def import
    tu = TwitterUser.find_by screen_name: /^#{@screen_name}$/i
    if tu.blank?
      throw :user_not_found
    end

    if tu.import_to_profile_done.blank? && tu.feed_links_parse_done && 
      TwitterUserToProfileImporter.condition(tu) && (not tu.feed_links.blank?)
      # remove comments and atom feeds (considering rss is always present)
      filtered_feed_links = filter_feed_links(tu.feed_links)

      if not filtered_feed_links.blank?
        profile = Profile.find_by(twitter_handle: /^#{@screen_name}$/i) || Proc.new do
          json_data = JSON.parse(tu.users_lookup_json_data)
          Profile.create({
            twitter_handle: @screen_name,
            name: tu.name,
            photo_url: json_data["profile_image_url"].gsub("_normal.", "_bigger."),
            short_description: json_data["description"],
            description: json_data["description"]
          })
        end.call

        feeds = filtered_feed_links.map do |feed_link|
          Feed.find_by(feed_url: feed_link["href"]) || Proc.new do
            Feed.create({title: feed_link["title"], feed_url: feed_link["href"]})
          end.call
        end

        feed_tokens = feeds.map{ |feed| feed.token }
        profile.feed_tokens ||= []
        profile.feed_tokens += feed_tokens
        profile.feed_tokens.uniq!
        profile.save

        tu.import_to_profile_done = true
        tu.save
      end
    end
  end

  def filter_feed_links(feed_links)
    not feed_links.blank? and without_comments_feed_link = feed_links.select do |feed_link|
      if feed_link["title"] && feed_link["title"].downcase.include?("comment")
        false
      elsif feed_link["href"].blank? || feed_link["href"].downcase.include?("comment")
        false
      else
        true
      end
    end

    # If contains both RSS and ATOM, filter out the ATOMS
    without_comments_feed_link.is_a?(Array) and without_comments_feed_link.count > 1 and 
      non_atom_links = without_comments_feed_link.select do |feed_link|
      if (not feed_link["title"].blank?) && feed_link["title"].downcase.include?("atom")
        false
      elsif feed_link["href"].downcase.include?("atom")
        false
      else
        true
      end
    end

    if non_atom_links.is_a?(Array) && non_atom_links.count > 0
      non_atom_links
    else
      without_comments_feed_link
    end
  end

  def self.condition(tu)
    (tu.name.split(/ +/).count == 2) ? true : false
  end
end

if __FILE__ == $0
  TwitterUser.all.each do |twitter_user|
    
    # Resque.enqueue(TwitterUserToProfileImporter, twitter_user.screen_name) if TwitterUserToProfileImporter.condition(twitter_user)
  end
end