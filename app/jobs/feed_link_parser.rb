class FeedLinkParser
  def initialize(url)
    @url = url
  end

  def parse
    @response = HTTParty.get(@url)
  end

  def response
    return @response
  end

  def feed_links
    doc = Nokogiri::HTML(@response.body)
    doc.css('link[type="application/rss+xml"]', 'link[type="application/atom+xml"]').map do |link|
      {title: link['title'], href: link['href']}
    end
  end
end