class CrawlLogger < Logger
  @@logger = nil

  def self.logger
    unless @@logger
      filepath = Rails.root + 'log/crawl.log'
      logfile = File.new(Rails.root + 'log/crawl.log', 'a')  #create log file
      logfile.sync = true  #automatically flushes data to file
      @@logger = CrawlLogger.new logfile
    end
    @@logger
  end

  def format_message(severity, timestamp, progname, msg)
    "#{timestamp.to_formatted_s(:db)} #{severity} #{msg}\n"
  end
end

CRAWL_LOGGER = CrawlLogger.logger