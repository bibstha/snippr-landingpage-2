# Background Jobs

## Scripts ##

**feed_updater.rb**

Goes through all rss feeds, crawls them and stores the latest in the database. Also checks if feeds have contents. If feeds do not have contents, the urls are put into a resque-queue: **feedcrawler**


## Redis Queues

** Feed Crawler **  
For given URL, downloads the main content and saves it as the content in database.

<!-- ** Content Crawler **
*  -->