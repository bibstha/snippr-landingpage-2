# Snippr Codebase

* [Background Jobs](wiki/Background Jobs)

## Installation instruction

    $ git pull git@bitbucket.org:bibstha/snippr-landingpage-2.git snippr
    $ cd snippr
    $ bundle install
    $ rails s

Make sure at the same time, mongod and redis are running

### Install Mongod

Check online for the install instructions.

Starting mongod

    $ sudo /etc/init.d/mongo start

Starting redis

    $ sudo /etc/init.d/redis start

Resque Queue Manager is responsible for downloading the feeds. Make sure it is running too

    $ QUEUE=feedcrawler,feed rake resque:work

## How to Use

Add authors

    go to `http://localhost:3000/admin/authors`.
    Type Author Name and RSS Feed
    Click Add

    go to `http://localhost:3000/authors`.
    Click on author name to see his posts