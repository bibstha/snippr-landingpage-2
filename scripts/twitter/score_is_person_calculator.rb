class ScoreIsPersonCalculator
  def calculate
    TwitterUser.all.each do |tu|
      # profile_data = JSON.parse(tu.users_lookup_json_data)
      # csv << [tu.user_id, tu.screen_name, tu.friends_count, tu.followers_count, profile_data["profile_image_url"], tu.feed_links]
      
      not tu.feed_links.blank? and filtered_feed_links = tu.feed_links.select do |feed_link|
        if feed_link["title"] && feed_link["title"].downcase.include?("comment")
          false
        elsif feed_link["href"] && tu.feed_links.count == 2 && feed_link["href"].downcase.include?("atom")
          false
        else
          true
        end
      end
      
      # csv << [tu.user_id, tu.screen_name, tu.name, tu.friends_count, tu.followers_count, tu.feed_links, filtered_feed_links, tu.expanded_url]
      
      json_data = JSON.parse(tu.users_lookup_json_data)
      puts "User: #{tu.screen_name} / #{json_data['name']}"

      ru = TwitterUserReport.find_by(user_id: tu.user_id) || TwitterUserReport.new
      ru.user_id = tu.user_id
      ru.screen_name = tu.screen_name
      ru.name = tu.name
      ru.friends_count = tu.friends_count
      ru.followers_count = tu.followers_count
      # ru.profile_image_url = 
      ru.feed_links = filtered_feed_links
      ru.has_feed_links = filtered_feed_links ? true : false

      # ru.score_is_person = user_score_calculator(tu.name)
      ru.save
    end
  end
end


calculator = ScoreIsPersonCalculator.new
calculator.calculate