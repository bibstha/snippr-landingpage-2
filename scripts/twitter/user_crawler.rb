require 'extensions/crawl_logger'

class UserCrawler
  def fetch_own_and_immediate_friends_profiles(screen_name)
    tu = TwitterUser.find_by(screen_name: screen_name)
    if tu
      CRAWL_LOGGER.debug("User found for #{screen_name} id #{tu.user_id}")
    else
      CRAWL_LOGGER.debug("Existing user not found for #{screen_name}, will try to pull from twitter")
      tu = TwitterUser.pull_users_lookup(screen_name, true)[0]
    end

    if tu.friends_ids_done
      CRAWL_LOGGER.debug("Friends already extracted for #{screen_name}, quitting...")
    else
      CRAWL_LOGGER.debug("Friends will be pulled")
      
      tu = TwitterUser.pull_friends_ids(tu.user_id)
      Resque.enqueue(TwitterUserFeedParser, tu.first.screen_name)
      # filter out all previously crawled ids
      filtered_friends_ids = tu.friends_ids.reject { |id| TwitterUser.users_lookup_done?(id) }
      CRAWL_LOGGER.debug("Total friends to be crawled for #{screen_name} = #{filtered_friends_ids.length}")
      filtered_friends_ids.each_slice(100) do |friends| # for 100 users, do a single query crawl
        CRAWL_LOGGER.debug("Extracting 100 users for #{screen_name}")
        pulled = TwitterUser.pull_users_lookup(friends.join(","))
        pulled.each do |twitter_user|
          Resque.enqueue(TwitterUserFeedParser, twitter_user.screen_name)
        end
        CRAWL_LOGGER.debug("Pulled #{pulled.length} data")
      end
    end
  end
end

if __FILE__ == $0
  UserCrawler.new.fetch_own_and_immediate_friends_profiles ARGV[0]
end