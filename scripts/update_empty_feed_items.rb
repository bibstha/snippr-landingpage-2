feed_items = FeedItem.where(content: nil)
feed_items.each do |feed_item|
  puts "Updating empty FeedItem token: %s" % feed_item.token
  Resque.enqueue(FeedItemIndetailExtractor, feed_item.token)
end