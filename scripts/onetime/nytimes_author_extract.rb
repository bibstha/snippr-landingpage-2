# NewYork Times Author Extractor
require 'nokogiri'
require 'open-uri'

def extract_author_list
  entry_url = "http://topics.nytimes.com/top/reference/timestopics/people/"
  doc = Nokogiri::HTML(open(entry_url))

  authors = doc.css('a.listingReporter').map do |link|
    {name: link.content.split(", ").reverse.join(" "), link: link.attr('href')}
  end
  puts "Found #{authors.count} Authors"

  authors.each do |author|
    puts "Starting for name=#{author[:name]}\nurl=#{author[:link]}"
    feed_url = "#{author[:link]}?rss=1"
    profile = Profile.find_by(name: author[:name])
    if profile
      puts "USER: Using existing user #{profile.name}"
    else
      author_info = extract_author_fields(author[:link])
      profile = Profile.create({
        name: author[:name],
        short_description: author_info[:content][0],
        description: author_info[:content].join("\n"),
        twitter_handle: author_info[:twitter],
        facebook_url: author_info[:facebook],
        temp: {nytimes_total_articles_written: author_info[:total_articles]}
      })
      puts "USER: New user #{profile.name} created"
    end

    feed = Feed.create_by_feed_url(feed_url)
    profile.feed_tokens ||= []
    profile.feed_tokens << feed.token unless profile.feed_tokens.include? feed.token
    profile.save
    puts "New Feed added #{feed.feed_url}"
  end
end

# returns a hash of {twitter:, facebook:, content}
def extract_author_fields url
  doc = Nokogiri::HTML(open(url))
  if not doc.css('.vcard').empty?
    result = doc.css('.vcard')[0].children.inject({twitter: '', facebook: '', content: []}) do |accum, vcard_elem|
      if vcard_elem.content != 'Read More...' && vcard_elem.content != 'Hide'
        if vcard_elem.content.include? "Twitter:"
          accum[:twitter] = vcard_elem.content.gsub(/Twitter:\s+@/, "")
        elsif vcard_elem.content.include? "on Facebook"
          accum[:facebook] = vcard_elem.children[0].attr('href')
        elsif vcard_elem.content.include? "Subscribe to"
        elsif vcard_elem.content == "\n"
        else
          # p "NAME IS", vcard_elem.name, vcard_elem.content
          accum[:content] << vcard_elem.content.gsub(/(\n)+(Hide)*/, "")
        end
      end
      accum
    end
  else
    description_tag = doc.css('#ledeModule')[0]
    if description_tag
      bio = description_tag.children.select do |x|
        not x.content.downcase.include? "send an e-mail to" and
        not x.content == "\n"
      end
      result = {twitter: '', facebook: '', content: bio.map {|x| x.content}}
    else
      result = {twitter: '', facebook: '', content: [""]}
    end
  end
  result[:total_articles] = doc.css('.srchHdr')[0] ? doc.css('.srchHdr')[0].content.gsub(/[^0-9]+/,"").to_i : 0
  result
end

extract_author_list
# p extract_author_fields "http://topics.nytimes.com/top/reference/timestopics/people/a/reed_abelson/index.html"
# p extract_author_fields "http://topics.nytimes.com/top/reference/timestopics/people/a/halimah_abdullah/index.html"
# p extract_author_fields "http://topics.nytimes.com/top/opinion/editorialsandoped/oped/columnists/frankbruni/index.html"
# p extract_author_fields "http://topics.nytimes.com/top/reference/timestopics/people/b/lisa_belkin/index.html"