require 'nokogiri'
require 'open-uri'
require "#{Rails.root}/scripts/onetime/school-crawler/article"
# require_relative 'article'
require 'digest/md5'


class ArticleCrawlerTemplate
  def parse
    @pagination_template = "http://knowledge.insead.edu/blogs/page/%s/0"
    @article_container_css_selector = ".region article.feat.comment"
    @page_array = 0..16

    total_articles = @page_array.inject(0) do |accum, page_number|
      puts "Crawling #{@pagination_template % page_number}"
      doc = Nokogiri::HTML(open(@pagination_template % page_number))
      articles = doc.css(@article_container_css_selector).map do |article_node|
        # puts article_node.css('.title')[0].content

        article = Article.new
        article.title = article_node.css('.title')[0].content
        article.url = "http://knowledge.insead.edu" + article_node.css('.title a')[0]['href']
        article.summary = article_node.css('p')[0].content
        article.content = ''
        article.author = article_node.css('.metas span a')[0].content if article_node.css('.metas span a')[0]
        article.author_links = [ "http://knowledge.insead.edu#{article_node.css('.metas span a')[0]['href']}" ] if article_node.css('.metas span a')[0]
        article.pub_date = article_node.css('.metas time')[0]["datetime"]
        article.entry_id = Digest::MD5.hexdigest(article.url)
        article.feed_token = ''
        article.tags = ['insead-blog']
        article.save
        article
      end

      accum += articles.count
    end
    puts "Total Articles: #{total_articles}"
  end
end

ArticleCrawlerTemplate.new.parse