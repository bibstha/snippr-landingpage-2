# Remove @from all twitter handles
def remove_at_from_twitter_handle
  profiles = Profile.where(twitter_handle: /@/)
  profiles.each do |profile|
    puts "Fixing twitter_handle for #{profile.twitter_handle}"
    profile.twitter_handle = profile.twitter_handle.from(1)
    profile.save
  end
end

# Find duplicate twitter_handle
def find_duplicate_twitter_handles
  twitter_handle_collection = Profile.all
  twitter_handle = twitter_handle_collection.map { |x| x.name }

  h = Hash.new(0)
  twitter_handle.each { | v | h.store(v, h[v]+1) }
  p h.select { | k, v | v > 1 }
end

def import_twitter_handle_missing_names_from_twitter_user
  profiles = Profile.where(twitter_handle: {"$ne" => nil})
  profiles.each do |profile|
    puts "Checking for #{profile.twitter_handle}"
    tu = TwitterUser.find_by(screen_name: profile.twitter_handle)
    if tu
      puts "Updating Name for #{tu.screen_name} as #{tu.name}"
      profile.name = tu.name
      profile.save
    end
  end
end

def fix_empty_profile_link
  TwitterUser.all.each do |tu|
    # puts tu.screen_name
    if not tu.filtered_feeds.blank?
      profile = Profile.find_by(twitter_handle: tu.screen_name) and Proc.new do
        if profile.feed_tokens.blank?
          feeds = tu.filtered_feeds
          feed_objs = Feed.where(feed_url: {"$in" => feeds.map {|x| x["href"]}})
          if not feed_objs.blank?
            profile.feed_tokens = feed_objs.map { |x| x.token }
            profile.save
          end
        else
          # puts "Not empty feed_tokens for #{profile.twitter_handle}"
        end
      end.call
    end
  end
end

if __FILE__ == $0
  method_name = ARGV[0]
  if method_name && defined?(method_name)
    send(method_name)
  else
    print "Not found : #{method_name}\n"
  end
end