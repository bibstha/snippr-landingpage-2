require 'nokogiri'
require 'open-uri'
require "#{Rails.root}/scripts/onetime/school-crawler/article"
# require_relative 'article'
require 'digest/md5'


class ArticleCrawlerTemplate
  def parse
    @base_path = "http://www.tuck.dartmouth.edu"
    @pagination_template = "http://www.tuck.dartmouth.edu/newsroom/faculty-forum/research/P%s"
    @article_container_css_selector = ".listingSection > ul > a"
    # @page_array = 0..9
    @page_array = 0..9

    total_articles = @page_array.inject(0) do |accum, page_number|
      crawl_url = @pagination_template % (page_number * 7)
      puts "Crawling #{crawl_url}"
      doc = Nokogiri::HTML(open(crawl_url))
      articles = doc.css(@article_container_css_selector).map do |article_node|
        # puts article_node.css('.title')[0].content

        article_url = @base_path + article_node["href"]
        article_doc = Nokogiri::HTML(open(article_url))

        article = Article.new
        article.title = article_node.css('li div h4')[0].content
        article.url = @base_path + article_node["href"]
        article.summary = article_node.css('li div p')[0].content
        article.content = article_doc.css('#newsroomMain .article .body > p').map{|x| x.content}.join("\n")
        author_and_pubdate = /By (.*)\n.*Published (.*)\n/.match(article_doc.css('#newsroomMain .article .body p')[0].content)
        article.author =  author_and_pubdate[1].split(",")[0].gsub(/[^a-zA-Z ]/, "") if author_and_pubdate[1].split(",")[0]
        article.pub_date = author_and_pubdate[2]
        article.entry_id = Digest::MD5.hexdigest(article.url)
        article.feed_token = ''
        article.tags = ['dartmouth-newsroom', 'dartmouth']
        article.author_tags = ['dartmouth-newsroom', 'dartmouth']
        article.save
        # p article.inspect
        article
      end

      accum += articles.count
    end
    puts "Total Articles: #{total_articles}"
  end
end

ArticleCrawlerTemplate.new.parse