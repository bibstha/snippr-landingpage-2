# To run this, go to Rails.root and execute
# rails runner scripts/onetime/columbia_global_insights.rb

require 'nokogiri'
require 'open-uri'
require "#{Rails.root}/scripts/onetime/school-crawler/article"
require 'digest/md5'

class IMDTomorrowChallenges
  def parse
    @pagination_template = "http://www.imd.org/research/challenges/archive.cfm"
    @article_container_css_selector = "#res2ResultsTable tr"          
    accum = 0 

    while @pagination_template.length > 0
      puts "Crawling #{@pagination_template}"   
      doc = Nokogiri::HTML(open(@pagination_template))   
      
      interesting_nodes = doc.css(@article_container_css_selector).select do |node|
        node.css('.recordRow').count > 0
      end

      articles = interesting_nodes.map do |article_node|
        

        title = article_node.children[2].css('a')[0].content.capitalize                         
        url = "http://www.imd.org" + article_node.children[2].css('a')[0]['href']                       
        summary = article_node.css('.alignedCopy').text if  article_node.css('.alignedCopy')                        
        author = article_node.children[4].content if article_node.children[5]              
        pub_date = article_node.children[0].content if article_node.children[0]                 
        entry_id = Digest::MD5.hexdigest(url)           
        articlepage = Nokogiri::HTML(open(url))
        content = articlepage.css('.NewsAndTCsCon1Container').text if  articlepage.css('.NewsAndTCsCon1Container')    
        if articlepage.css('.NewsAndTCsCon1Container p em a')[0]
          author_links= articlepage.css('.NewsAndTCsCon1Container p em a')[0]['href'] 
        elsif articlepage.css('.NewsAndTCsCon1Container p span a')[0]
          author_links = articlepage.css('.NewsAndTCsCon1Container p span a')[0]['href']
        else
          author_links = ''
        end         
       
              
        tags = ['imd-tomorrows-challenges','imd'] 

        
        
        # content = ''
        

        # print values for debugging
        
        puts title
        puts url
        puts summary
        puts author        
        puts pub_date
        puts content 
        puts author_links 
        puts tags   

        #insert into db

        article = Article.new
        article.title = title
        article.url = url
        article.summary = summary
        article.content = content
        article.author = author
        article.author_tags = tags
        article.pub_date = pub_date
        article.author_links = author_links
        article.entry_id = entry_id
        article.feed_token = ''
        article.tags = tags        
        article.save
        article

        accum += 1
      end

      if URI.encode(doc.css(".flipNavNext a")[0]['href'])
        @pagination_template = "http://www.imd.org/research/challenges/" + URI.encode(doc.css(".flipNavNext a")[0]['href'])
      else
        @pagination_template = ''
        break
      end 
        
      
    end
    puts "Total Articles: #{accum}"
  end
end

IMDTomorrowChallenges.new.parse