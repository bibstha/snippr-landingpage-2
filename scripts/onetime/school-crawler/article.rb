require 'time'

class Article
  def title=(title)
    @title = title.strip if title
  end

  def summary=(summary)
    @summary = summary.strip if summary
  end

  def content=(content)
    @content = content
  end

  def pub_date=(pub_date)
    @pub_date = Time.parse(pub_date) if pub_date
  end

  def feed_token=(feed_token)
    @feed_token = feed_token
  end

  def url=(url)
    @url = url
  end

  def url
    @url
  end

  def author=(author)
    @author = author ? author.strip : ""
  end

  def author_description=(author_description)
    @author_description = author_description
  end

  def author_links=(author_links)
    @author_links = author_links.is_a?(Array) ? author_links : [author_links]
  end

  def author_tags=(author_tags)
    @author_tags = author_tags
  end

  def entry_id=(entry_id)
    @entry_id = entry_id
  end

  def tags=(tags)
    @tags = tags
  end

  def save
    # p self.inspect
    puts "Saving #{@title}"
    values = {
      name: @author,
      web_urls: @author_links,
      short_description: @author_description,
      description: @author_description,
      tags: @author_tags,
    }
    profile = Profile.find_by(name: @author)
    if profile
      profile.update_attributes(values)
    else
      profile = Profile.create(values)
    end
    feed_item = FeedItem.find_by(url: @url)
    feed_values = {
      title: @title,
      url: @url,
      summary: @summary,
      content: @content,
      published: @pub_date,
      entry_id: @entry_id,
      tags: @tags,
      feed_token: [@feed_token],
      author: @author
    }
    if feed_item
      puts "Already Saved! Updating:: #{@url}"
      feed_item.update_attributes(feed_values)
      if not feed_item.profile_tokens.include? profile.token
        puts "Saving new author in existing feed_item"
        feed_item.profile_tokens ||= []
        feed_item.profile_tokens << profile.token
        feed_item.save
      end
    else
      puts "Creating new one. Saving..."
      feed_values[:profile_tokens] = [profile.token]
      feed_item = FeedItem.create(feed_values)
    end
  end
end