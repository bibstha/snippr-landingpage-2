require 'nokogiri'
require 'open-uri'
require "#{Rails.root}/scripts/onetime/school-crawler/article"
# require_relative 'article'
require 'digest/md5'


class ArticleCrawlerTemplate
  def parse
    @base_uri = "http://hbswk.hbs.edu"
    @faculty_page = "http://hbswk.hbs.edu/faculty/"
    faculty_doc = Nokogiri::HTML(open(@faculty_page))
    
    faculties = faculty_doc.css('.nu > a').map do |faculty_a_tag|
      { name: faculty_a_tag.content, url: "http://hbswk.hbs.edu#{faculty_a_tag['href']}" }
    end

    faculties = faculties.select{|x| x[:url].include? "faculty" }

    total_articles = faculties.inject(0) do |sum, faculty|
      puts "Crawling #{faculty[:url]}"
      faculty_article_doc = Nokogiri::HTML(open(faculty[:url]))
      @author_web_url = faculty_article_doc.css('h1 a')[0]['href']
      faculty_article_doc.css('.media').each do |article_node|
        article = Article.new
        article.title = article_node.css('h4 a')[0].content
        article.url = @base_uri + article_node.css('h4 a')[0]['href']
        article.summary = article_node.css('p')[0].children[0].content
        article.content = ""
        article.author = faculty[:name]
        article.author_tags = ['havard-business-school']
        article.author_links = [@author_web_url, faculty[:url]]
        article.pub_date = article_node.css('ul li')[0].content
        article.entry_id = Digest::MD5.hexdigest(article.url)
        article.feed_token = ''
        article.tags = ['havard-business-school']
        # puts article.inspect
        article.save
      end
      sum + faculty_article_doc.css('.media').count
    end

    puts "Total Articles: #{total_articles}"
    # @base_path = ".nu > a"
    # @pagination_template = "http://hbswk.hbs.edu/faculty/%s"
    # @article_container_css_selector = ".listingSection > ul > a"
    # # @page_array = 0..9
    # @page_array = 0..9

    # total_articles = @page_array.inject(0) do |accum, page_number|
    #   crawl_url = @pagination_template % (page_number * 7)
    #   puts "Crawling #{crawl_url}"
    #   doc = Nokogiri::HTML(open(crawl_url))
    #   articles = doc.css(@article_container_css_selector).map do |article_node|
    #     # puts article_node.css('.title')[0].content

    #     article_url = @base_path + article_node["href"]
    #     article_doc = Nokogiri::HTML(open(article_url))

    #     article = Article.new
    #     article.title = article_node.css('li div h4')[0].content
    #     article.url = @base_path + article_node["href"]
    #     article.summary = article_node.css('li div p')[0].content
    #     article.content = article_doc.css('#newsroomMain .article .body > p').map{|x| x.content}.join("\n")
    #     author_and_pubdate = /By (.*)\n.*Published (.*)\n/.match(article_doc.css('#newsroomMain .article .body p')[0].content)
    #     article.author =  author_and_pubdate[1].split(",")[0].gsub(/[^a-zA-Z ]/, "") if author_and_pubdate[1].split(",")[0]
    #     article.pub_date = author_and_pubdate[2]
    #     article.entry_id = Digest::MD5.hexdigest(article.url)
    #     article.feed_token = ''
    #     article.tags = ['dartmouth-newsroom', 'dartmouth']
    #     article.author_tags = ['dartmouth']
    #     article.save
    #     # p article.inspect
    #     article
    #   end

    #   accum += articles.count
    # end
    # puts "Total Articles: #{total_articles}"
  end
end

ArticleCrawlerTemplate.new.parse