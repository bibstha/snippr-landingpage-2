# To run this, go to Rails.root and execute
# rails runner scripts/onetime/columbia_global_insights.rb

require 'nokogiri'
require 'open-uri'
require "#{Rails.root}/scripts/onetime/school-crawler/article"
require 'digest/md5'

class ColumbiaGlobalInsight
  def parse
    @pagination_template = "http://www8.gsb.columbia.edu/chazen/globalinsights/?page=%s"
    @article_container_css_selector = ".article"
    @page_array = 0..12

    puts @page_array

    total_articles = @page_array.inject(0) do |accum, page_number|
      puts "Crawling #{@pagination_template % page_number}"
      doc = Nokogiri::HTML(open(@pagination_template % page_number))
      articles = doc.css(@article_container_css_selector).map do |article_node|
        # puts article_node.css('.title')[0].content 

        title = article_node.css('h2')[0].content        
        url = "http://www8.gsb.columbia.edu" + article_node.css('h2 a')[0]['href']        
        summary = article_node.css('p')[0].content       
        author = article_node.css('.author')[0].content if article_node.css('.author')[0]
        author = author.strip if author

        if not author or author == ""
            author = article_node.css('.author')[1].content if article_node.css('.author')[1]
        end
        pub_date = article_node.css('.date')[0].content if article_node.css('.date')[0]
        entry_id = Digest::MD5.hexdigest(url)
        

        articlepage = Nokogiri::HTML(open(url))
        content = articlepage.css('.article-text p').text          
        tags = ['columbia-global-insights','columbia'] + articlepage.css('.a').map{|x| x.content}

        article = Article.new
        article.title = title
        article.url = url
        article.summary = summary
        article.content = content
        article.author = author
        article.author_tags = ['columbia-global-insights','columbia']
        article.pub_date = pub_date
        article.entry_id = entry_id
        article.feed_token = ''
        article.tags = tags        
        article.save
        article
      end
      accum += articles.count
    end
    puts "Total Articles: #{total_articles}"
  end
end

ColumbiaGlobalInsight.new.parse