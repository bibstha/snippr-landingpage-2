# To run this, go to Rails.root and execute
# rails runner scripts/onetime/columbia_global_insights.rb

require 'nokogiri'
require 'open-uri'
require "#{Rails.root}/scripts/onetime/school-crawler/article"
require 'digest/md5'

class KellogBlogs
  def parse
    @pagination_template = "http://insight.kellogg.northwestern.edu/blogs/_entries/P%s"
    @article_container_css_selector = ".tz-mod"
    @page_array = 0..0
    page_number = 0
    last_page = 540
    accum = 0
   

    puts @page_array

    until page_number > last_page
      puts "Crawling #{@pagination_template % page_number}"
      doc = Nokogiri::HTML(open(@pagination_template % page_number))
      articles = doc.css(@article_container_css_selector).map do |article_node|
        # puts article_node.css('.title')[0].content 

        title = article_node.css('.tz-h3')[0].content                     
        url = article_node.css('.tz-h3 a')[0]['href'] 

        if url.length == 0                   
          next
        end 

        summary = article_node.css('.tz-txt').text if  article_node.css('.tz-txt').text                 
        author = article_node.css('.byline')[0].content if article_node.css('.byline')[0]        
        # pub_date = article_node.css('.tz-time')[0].content if article_node.css('.tz-time')[0]  
        date = url.match(/([0-9]{4})\/([0-9]{1,2})\/([0-9]{1,2})/)         
        if date
          pub_date = date[0] 
        else
          pub_date = '1970/10/10' 
        end             

        author_links = URI(url).host       
        entry_id = Digest::MD5.hexdigest(url)           
        content = ''       
        tags = ['kellog-blogs','kellog'] 

        
        
        # content = ''
        

        # print values for debugging
        
        # puts title
        # puts url
        # puts summary
        # puts author
        # puts page_number
        # puts pub_date
        # puts content 
        # puts author_links 
        # puts tags   

        #insert into db

        article = Article.new
        article.title = title
        article.url = url
        article.summary = summary
        article.content = content
        article.author = author
        article.author_tags = tags
        article.pub_date = pub_date
        article.author_links = author_links
        article.entry_id = entry_id
        article.feed_token = ''
        article.tags = tags
        article.save
        article

        accum += 1
      end
      page_number += 20
    end
    puts "Total Articles: #{accum}"
    
  end
end

KellogBlogs.new.parse