require 'nokogiri'
require 'open-uri'
require_relative 'article'
require 'digest/md5'

class BoothCapitalIdeas
  def parse
    @pagination_template = "http://www.chicagobooth.edu/capideas/"
    @article_container_css_selector = ".block"
    @page_array = 0..0

    puts @page_array

    total_articles = @page_array.inject(0) do |accum, page_number|
      doc = Nokogiri::HTML(open(@pagination_template % page_number))
      articles = doc.css(@article_container_css_selector).map do |article_node|
        # puts article_node.css('.title')[0].content 

        if article_node.css('h1')[0]
          title = article_node.css('h1')[0].content 
          url = article_node.css('h1 a')[0]['href'] 
        else
          title = article_node.css('h2')[0].content
          url = article_node.css('h2 a')[0]['href']
        end                
        
        summary = article_node.css('p')[0].content  if article_node.css('p')[0]                        
        # pub_date = article_node.css('.date')[0].content unless article_node.css('.date').nil?
        entry_id = Digest::MD5.hexdigest(url)
        

        if url.include? "www.chicagobooth.edu/capideas/"
          url = url
        else
          url = "http://www.chicagobooth.edu" + url       
        end

        
        articlepage = Nokogiri::HTML(open(url)) rescue next
        content = articlepage.css('.article_content')[0].content

        if articlepage.css('#ContentPlaceHolder1_pnlResearchOn')[0]
            author = articlepage.css('#ContentPlaceHolder1_pnlResearchOn')[0].content
        elsif articlepage.css('.article_content p')[0]
            author = articlepage.css('.article_content p').map{|x| x.content}.last
            puts url
            puts author
        else
          next
        end       
        # tags = ['booth-capital-ideas'] 
        
        # content = ''
        

        # print values for debugging
        
        # puts title
        # puts url
        # puts summary
        # puts author
        # puts page_number
        # puts pub_date
        # puts content  
        # puts tags   

        #insert into db

        # article = Article.new
        # article.title = title
        # article.url = url
        # article.summary = summary
        # article.content = content
        # article.author = author
        # article.pub_date = pub_date
        # article.entry_id = entry_id
        # article.feed_token = ''
        # article.tags = tags
        # article.save
        #article

      end
      accum += articles.count
    end
    puts "Total Articles: #{total_articles}"
  end
end

BoothCapitalIdeas.new.parse