# To run this, go to Rails.root and execute
# rails runner scripts/onetime/columbia_global_insights.rb

require 'nokogiri'
require 'open-uri'
require "#{Rails.root}/scripts/onetime/school-crawler/article"
require 'digest/md5'

class MitSloanPublications
  def parse
    @pagination_template = "http://mitsloan.mit.edu/faculty/news/publications.php"
    @article_container_css_selector = ".standardtable tbody tr td"
    @page_array = 0..0

    puts @page_array

    total_articles = @page_array.inject(0) do |accum, page_number|
      puts "Crawling #{@pagination_template % page_number}"
      doc = Nokogiri::HTML(open(@pagination_template))
      articles = doc.css(@article_container_css_selector).map do |article_node|
        # puts article_node.css('.title')[0].content 

        title = article_node.css('p strong em')[0].content                      
        url = article_node.css('p strong em a')[0]['href']                      
        summary = article_node.css('p')[2].content if  article_node.css('p')[2]
        author = article_node.css('p')[0].children[2].content if article_node.css('p')[0].children[2].content        
        # pub_date = article_node.css('.views-field-field-source-publication')[0].content if article_node.css('.views-field-field-source-publication')[0]
        #pub_date = article_node.css('.date-published')[0].content if article_node.css('.date-published')[0]
        entry_id = Digest::MD5.hexdigest(url)   
        pub_date = '1970-12-12' 
        content = ''   

                
                        
        tags = ['mitsloan-publication','mit-sloan']  
        
       
        

        # print values for debugging
        
        # puts title
        # puts url
        # puts summary
        # puts author
        # puts page_number
        # puts pub_date
        # puts content  
        # puts tags   

        #insert into db

        article = Article.new
        article.title = title
        article.url = url
        article.summary = summary
        article.content = content
        article.author = author
        article.author_tags = tags
        article.pub_date = pub_date
        article.entry_id = entry_id
        article.feed_token = ''
        article.tags = tags
        article.save
        article
      end
      accum += articles.count
    end
    puts "Total Articles: #{total_articles}"
  end
end

MitSloanPublications.new.parse