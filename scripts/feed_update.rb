#! env ruby
if ARGV.length > 0

  user_token = ARGV[0]
  profile = Profile.find(user_token)
  unless profile
    puts "Did not find Profile with user token: %s" % user_token
    exit
  end
  puts "Updating feed for %s" % profile.name
  feeds = Feed.where(:token => {"$in" => profile.feed_tokens})
  feeds.each do |feed|
    puts "Updating Feed %s" % feed.title
    feed.pull
  end
else
  puts "dummy call to feed_update_all"
end