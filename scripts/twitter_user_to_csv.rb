require 'csv'

class TwitterUserToCsv
  def initialize(output_file_path)
    @output_file_path = output_file_path
  end

  def write
    CSV.open(@output_file_path, "w") do |csv|
      csv << ["user_id", "screen_name", "name", "friends_count", "followers_count", "feed_links", "feed_filtered", "expanded_url"]
      TwitterUser.all.each do |tu|
        # profile_data = JSON.parse(tu.users_lookup_json_data)
        # csv << [tu.user_id, tu.screen_name, tu.friends_count, tu.followers_count, profile_data["profile_image_url"], tu.feed_links]
        
        not tu.feed_links.blank? and filtered_feed_links = tu.feed_links.select do |feed_link|
          if feed_link["title"] && feed_link["title"].downcase.include?("comment")
            false
          elsif feed_link["href"] && tu.feed_links.count == 2 && feed_link["href"].downcase.include?("atom")
            false
          else
            true
          end
        end
        
        csv << [tu.user_id, tu.screen_name, tu.name, tu.friends_count, tu.followers_count, tu.feed_links, filtered_feed_links, tu.expanded_url ]
      end
    end
  end
end

if __FILE__ == $0
  file_name = "output.csv"
  converter = TwitterUserToCsv.new(file_name)
  converter.write
end