if ARGV[0] == "feeds"
  feeds = Feed.all
  feeds.each do |feed|
    if not feed.feed_url
      puts "Deleting feed with token %s" % feed.token
      feed.delete
    else
      puts "Feed good %s" % feed.token
    end
  end

elsif ARGV[0] == "author_feed_items"
  if ARGV[1].blank?
    puts "token missing. Syntax: author_feed_items token"
    exit
  end

  author = Profile.find(ARGV[1])
  puts "Deleting feeds for %s (Token: %s)" % [author.name, author.token]
  feed_items = FeedItem.where(feed_token: {"$in" => author.feed_tokens})
  feed_items.each do |feed_item|
    puts "Deleting feeditem token:%s" % feed_item.token
    feed_item.delete
  end
else
  puts "command invalid"
end    