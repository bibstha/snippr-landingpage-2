require 'test_helper'

class FeedControllerTest < ActionController::TestCase
  test "should get author_list" do
    get :author_list
    assert_response :success
  end

  test "should get author_show_posts" do
    get :author_show_posts
    assert_response :success
  end

  test "should get author_search" do
    get :author_search
    assert_response :success
  end

end
