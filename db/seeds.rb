# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



if not Feed.all.count > 0
  feed = Feed.create(
    {
      title: "Paul Graham Essays",
      url: "http://paulgraham.com/articles.html",
      feed_url: "http://www.aaronsw.com/2002/feeds/pgessays.rss",
    }
  )

  profile_paul_graham = Profile.create(
    {
      name: "Paul Graham",
      photo_url: "https://si0.twimg.com/profile_images/1824002576/pg-railsconf.jpg",
      wikipedia_url: "http://en.wikipedia.org/wiki/Paul_Graham_(computer_programmer)",
      feed_tokens: [feed.token]
    }
  )
 
  FeedItem.create([
    {
      title: "Programming Bottom-Up",
      url: "http://www.paulgraham.com/progbot.html",
      feed_token: feed.token
    }
  ])
end

if User.all.count < 1
  Profile.create([
    {twitter_handle: "biancawelds"
    }])
  
  bianca = Profile.find_by(twitter_handle: "biancawelds")
  User.create([
    {
      display_name: "bibstha",
      email: "bibekshrestha@gmail.com",
      password: "Snippr88!"
    },
    {
      display_name: "bimal",
      email: "hakubimal@gmail.com",
      password: "Snippr88!",
      readlist_tokens: [],
      following_profiles: [bianca.token]
    },
    {
      display_name: "rajeev",
      email: "rajeevamatya@gmail.com",
      password: "Snippr88!"
    }
  ]);
end